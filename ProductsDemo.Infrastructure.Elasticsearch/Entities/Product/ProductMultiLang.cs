namespace ProductsDemo.Infrastructure.Elasticsearch.Entities.Product;

public record ProductMultiLang(int Id, string Name, string NameEn, string CategoryId, string Category,
    string CategoryEn, DateTimeOffset CreatedAt,
    DateTimeOffset? ModifiedAt,
    IReadOnlyCollection<ProductMultiLangField>? Fields);