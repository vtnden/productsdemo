namespace ProductsDemo.UseCases.Interfaces.Requests;

/// <summary>
/// Interface for all queries
/// </summary>
public interface IQueryRequest: IRequest {

}