using Elastic.Clients.Elasticsearch;
using Mapster;
using ProductsDemo.Domain.Common;
using ProductsDemo.Infrastructure.Elasticsearch.Constants;
using ProductsDemo.Infrastructure.Elasticsearch.Entities.ProductCategory;
using ProductsDemo.Infrastructure.Elasticsearch.Extensions;
using ProductsDemo.UseCases.Handlers.ProductsCategories.Dto;
using ProductsDemo.UseCases.Interfaces.Services;

namespace ProductsDemo.Infrastructure.Elasticsearch.Services;

public class ProductCategoryService: IProductCategoryService {
    private readonly ElasticsearchClient _client;

    public ProductCategoryService(ElasticsearchClient client) {
        _client = client;
    }

    public async Task<Result<ProductCategoryDto?>> Get(string id) {
        var response = await _client.GetAsync<ProductCategoryMultiLang>(id);
        return response.ToResult(response.Source.Adapt<ProductCategoryDto?>());
    }

    public async Task<IReadOnlyCollection<Result<bool>>> Create(IReadOnlyCollection<ProductCategoryDto> categories) {
        var response = await _client.BulkAsync(b => {
            foreach (var category in categories) {
                b.Index(category.Adapt<ProductCategoryMultiLang>());
            }
        });

        return response.Items.Select(x => x.ToResult(true)).ToList();
    }

    public async Task<Result<IReadOnlyCollection<ProductCategoryDto>>> Search(string? text) {
        if (string.IsNullOrEmpty(text)) {
            var response = await _client.SearchAsync<ProductCategoryMultiLang>(q => q.Query(q => q.MatchAll()));
            return response.ToResult(response.Documents.Adapt<IReadOnlyCollection<ProductCategoryDto>>());
        }

        var searchResponse = await _client.SearchTemplateAsync<ProductCategoryMultiLang>(
            st =>
                st.Indices(IndexName.From<ProductCategoryMultiLang>())
                    .Id(QueryConstants.ProductCategorySearchScriptName)
                    .Params(p => p.Add(QueryConstants.ProductCategorySearchScriptTextParamName, text))
        );

        return searchResponse.ToResult(searchResponse.Hits.Hits.Select(x => x.Source)
            .Adapt<IReadOnlyCollection<ProductCategoryDto>>());

    }

    public async Task<IReadOnlyCollection<Result<bool>>> Update(IReadOnlyCollection<ProductCategoryDto> categories) {
        var response = await _client.BulkAsync(b => {
            foreach (var category in categories) {
                b.Update<ProductCategoryMultiLang>(u =>
                    u.Index<ProductCategoryMultiLang>().Id(new Id(category.Id))
                        .Doc(category.Adapt<ProductCategoryMultiLang>()));
            }
        });

        return response.Items.Select(x => x.ToResult(true)).ToList();
    }

    public async Task<IReadOnlyCollection<Result<bool>>> Delete(IReadOnlyCollection<string> categoryIds) {
        var response = await _client.BulkAsync(b => {
            foreach (var id in categoryIds) {
                b.Delete<ProductCategoryMultiLang>(x => x.Id(id).Index<ProductCategoryMultiLang>());
            }
        });

        return response.Items.Select(x => x.ToResult(true)).ToList();
    }
}