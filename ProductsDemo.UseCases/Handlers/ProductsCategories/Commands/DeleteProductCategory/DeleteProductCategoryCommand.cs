using ProductsDemo.UseCases.Interfaces.Requests;

namespace ProductsDemo.UseCases.Handlers.ProductsCategories.Commands.DeleteProductCategory;

public record DeleteProductCategoryCommand(string Id): ICommandRequest;