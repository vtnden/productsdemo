using ProductsDemo.Domain.CommonEntities;
using ProductsDemo.UseCases.Handlers.ProductsCategories.Dto;
using ProductsDemo.UseCases.Interfaces.Requests.ProductCategory;

namespace ProductsDemo.UseCases.Handlers.ProductsCategories.Commands.UpdateProductCategory;

public record UpdateProductCategoryCommand
    (string CategoryId, UpdateProductCategoryDto Value): IEditProductCategoryCommand {
    public string Id => CategoryId;
    public string Name => Value.Name;
    public IReadOnlyCollection<ProductCategoryField>? Fields => Value.Fields;
}