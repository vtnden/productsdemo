using Elastic.Clients.Elasticsearch;
using Elastic.Clients.Elasticsearch.Analysis;
using Elastic.Clients.Elasticsearch.IndexManagement;
using Polly;
using ProductsDemo.Domain.Constants;
using ProductsDemo.Infrastructure.Elasticsearch.Entities.ProductCategory;
using ProductsDemo.Infrastructure.Elasticsearch.Extensions;
using ExistsResponse = Elastic.Clients.Elasticsearch.IndexManagement.ExistsResponse;

namespace ProductsDemo.Infrastructure.Elasticsearch.Migrations.ProductCategory;

public static class ProductCategoryIndexMigration {
    public static void CheckProductCategoryIndex(ElasticsearchClientSettings settings, ElasticsearchClient client) {
        var indexName = Environment.GetEnvironmentVariable(EnvConstants.ElasticProductsCategoriesIndexName)!;

        settings.DefaultMappingFor<ProductCategoryMultiLang>(mapping => {
            mapping.IdProperty(x => x.Id);
            mapping.IndexName(indexName);
        });

        Policy.Handle<Exception>()
            .OrResult<(ExistsResponse, CreateIndexResponse)>(r => !r.Item1.IsSuccess())
            .WaitAndRetry(new[] { TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(5), TimeSpan.FromSeconds(5) })
            .Execute(() => {
                var existResponse =
                    client.Indices.Exists(
                        new Elastic.Clients.Elasticsearch.IndexManagement.ExistsRequestDescriptor<
                            ProductCategoryMultiLang>(
                            indexName));

                var createResponse = client.CreateProductCategoryIndex(indexName);

                return (existResponse, createResponse);
            });

    }

    private static CreateIndexResponse CreateProductCategoryIndex(this ElasticsearchClient client, string indexName) {
        return client.Indices.Create<ProductCategoryMultiLang>(indexName,
            index => {
                index.Settings(s => s.AddAnalyzers(Language.Russian, Language.English));
                index.Mappings(mapping => {
                    mapping.Properties(props => {
                        props.Keyword(x => x.Id);
                        props.Text(x => x.Name, x => x.UseAnalyzer(Language.Russian));
                        props.Text(x => x.NameEn, x => x.UseAnalyzer(Language.English));
                        props.Date(x => x.CreatedAt);
                        props.Date(x => x.ModifiedAt!);
                        props.Nested(x => x.Fields!,
                            n => n.Properties(np =>
                                np.Keyword(npp => npp.Fields!.First().Id)
                                    .Keyword(npp => npp.Fields!.First().FieldType)
                                    .Text(npp => npp.Fields!.First().Name, x => x.UseAnalyzer(Language.Russian))
                                    .Text(npp => npp.Fields!.First().NameEn, x => x.UseAnalyzer(Language.English))
                                    .Boolean(npp => npp.Fields!.First().Required)
                                    .Nested(npp => npp.Fields!.First().Props!,
                                        nppp => nppp.Properties(
                                            npn => npn.Keyword(
                                                    npnn => npnn.Fields!.First().Props!.First().FieldPropType)
                                                .Keyword(npnn => npnn.Fields!.First().Props!.First().Value!)
                                        )
                                    )
                            )
                        );
                    });
                });
            });

    }
}