using Moq;
using ProductsDemo.Domain.Common;
using ProductsDemo.UseCases.Handlers.ProductsCategories.Dto;
using ProductsDemo.UseCases.Interfaces.Services;
using ProductsDemo.UseCases.Interfaces.Validators;

namespace ProductsDemo.UnitTests.Utils;

public static class MockUtils {
    public static IProductCategoryService MockProductCategoryServiceWithDto(Result<ProductCategoryDto?> dto) {
        var mockService = new Mock<IProductCategoryService>();
        mockService.Setup(x => x.Get(It.IsAny<string>())).ReturnsAsync((string id) =>
            dto);
        return mockService.Object;
    }

    public static IValidationContext<TResult> MockValidationContext<TResult>() {
        TResult context = default;
        var mock =
            new Mock<IValidationContext<TResult>>();
        mock.SetupProperty(x => x.Value);
        return mock.Object;
    }
}