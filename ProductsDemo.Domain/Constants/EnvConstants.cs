namespace ProductsDemo.Domain.Constants;

public class EnvConstants {
    public const string AspNetCoreEnv = "ASPNETCORE_ENVIRONMENT";
    public const string DotNetEnv = "DOTNET_ENVIRONMENT";
    public const string DbHost = "DB_HOST";
    public const string DbPort = "DB_PORT";
    public const string DbUser = "DB_USER";
    public const string DbPassword = "DB_PASSWORD";
    public const string DbName = "DB_NAME";

    public const string ElasticHost = "ELASTIC_HOST";
    public const string ElasticUserName = "ELASTIC_USERNAME";
    public const string ElasticUserPassword = "ELASTIC_PASSWORD";
    public const string ElasticProductsIndexName = "ELASTIC_PRODUCTS_INDEX_NAME";
    public const string ElasticProductsCategoriesIndexName = "ELASTIC_PRODUCTS_CATEGORIES_INDEX_NAME";

}