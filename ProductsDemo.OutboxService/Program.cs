using NLog.Extensions.Logging;
using ProductsDemo.Infrastructure.Elasticsearch;
using ProductsDemo.Infrastructure.Postgres;
using ProductsDemo.OutboxService.Workers;

var host = Host.CreateDefaultBuilder(args)
    .ConfigureServices(
        services => {
            PostgresModule.Init(services);
            ElasticsearchModule.Init(services);

            services.AddHostedService<ProductWorker>();
            services.AddHostedService<ProductCategoryWorker>();
        })
    .ConfigureLogging((context, logging) => {
        logging.ClearProviders();
        logging.AddNLog(context.Configuration, new NLogProviderOptions() { LoggingConfigurationSectionName = "NLog" });
    })
    .Build();

host.Run();