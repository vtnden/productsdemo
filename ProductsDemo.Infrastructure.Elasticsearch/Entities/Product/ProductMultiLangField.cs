using ProductsDemo.Domain.Enums;

namespace ProductsDemo.Infrastructure.Elasticsearch.Entities.Product;

public record ProductMultiLangField(bool Required, FieldType FieldType, string FieldId, string Field, string FieldEn,
    string? Value, string? ValueEn);