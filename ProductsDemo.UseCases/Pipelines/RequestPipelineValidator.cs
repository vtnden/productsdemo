using FluentValidation;
using ProductsDemo.Domain.Common;
using ProductsDemo.Domain.Enums;
using ProductsDemo.UseCases.Interfaces.Handlers;
using ProductsDemo.UseCases.Interfaces.Requests;

namespace ProductsDemo.UseCases.Pipelines;

/// <summary>
/// Common pipeline decorator for requests
/// </summary>
/// <typeparam name="TRequest"></typeparam>
/// <typeparam name="TResult"></typeparam>
public class RequestPipelineValidator<TRequest, TResult>: IPipelineHandler<TRequest, TResult>
    where TRequest : IRequest {
    private readonly IEnumerable<IValidator<TRequest>> _validators;
    private readonly IRequestHandler<TRequest, TResult> _handler;

    public RequestPipelineValidator(IEnumerable<IValidator<TRequest>> validators,
        IRequestHandler<TRequest, TResult> handler) {
        _validators = validators;
        _handler = handler;
    }

    public async Task<Result<TResult>> Send(TRequest request, CancellationToken cancellationToken = default) {
        var errors = new Dictionary<string, string[]>();
        foreach (var validator in _validators) {
            var validationResult = await validator.ValidateAsync(request, cancellationToken);
            if (validationResult.IsValid) {
                continue;
            }
            foreach (var (key, value) in validationResult.ToDictionary()) {
                errors.TryAdd(key, value);
            }
        }

        if (errors.Count > 0) {
            return Result<TResult>.FromError(ErrorType.IncorrectInputData, errors);
        }

        return await _handler.Send(request, cancellationToken);
    }
}