using ProductsDemo.Domain.Common;
using ProductsDemo.UseCases.Handlers.ProductsCategories.Dto;
using ProductsDemo.UseCases.Interfaces.Handlers;
using ProductsDemo.UseCases.Interfaces.Services;

namespace ProductsDemo.UseCases.Handlers.ProductsCategories.Queries.GetProductCategory;

public class GetProductCategoryHandler: IQueryHandler<GetProductCategoryQuery, ProductCategoryDto?> {
    private readonly IProductCategoryService _service;

    public GetProductCategoryHandler(IProductCategoryService service) {
        _service = service;
    }

    public Task<Result<ProductCategoryDto?>> Send(GetProductCategoryQuery request,
        CancellationToken cancellationToken = default) {
        return _service.Get(request.Id);
    }
}