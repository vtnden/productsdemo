using System.Text.Json;
using System.Transactions;
using Mapster;
using Microsoft.EntityFrameworkCore;
using ProductsDemo.Domain.Common;
using ProductsDemo.Domain.CommonEntities;
using ProductsDemo.Domain.Constants;
using ProductsDemo.Domain.Entities;
using ProductsDemo.Domain.Enums;
using ProductsDemo.UseCases.Handlers.Products.Dto;
using ProductsDemo.UseCases.Handlers.ProductsCategories.Dto;
using ProductsDemo.UseCases.Interfaces.DB;
using ProductsDemo.UseCases.Interfaces.Handlers;
using ProductsDemo.UseCases.Interfaces.Validators;

namespace ProductsDemo.UseCases.Handlers.Products.Commands.UpdateProduct;

/// <summary>
/// Handler for updating product
/// </summary>
public class UpdateProductHandler: ICommandHandler<UpdateProductCommand, bool> {
    private readonly IRwDbContext _dbContext;

    private readonly IValidationContext<(Dictionary<string, ProductCategoryField>? dict, ProductCategoryDto category)?>
        _validationContext;


    public UpdateProductHandler(
        IValidationContext<(Dictionary<string, ProductCategoryField>?, ProductCategoryDto)?> validationContext,
        IRwDbContext dbContext) {
        _validationContext = validationContext;
        _dbContext = dbContext;
    }

    public async Task<Result<bool>> Send(UpdateProductCommand request, CancellationToken cancellationToken = default) {
        await using var tr = await _dbContext.CreateTransactionScope(IsolationLevel.ReadCommitted);
        var modifiedAt = DateTimeOffset.Now;

        var currentProduct = await _dbContext.Products.Where(x => x.Id == request.Id && x.DeletedAt == null)
            .Select(x => new { x.CreatedAt }).FirstOrDefaultAsync(cancellationToken: cancellationToken);

        if (currentProduct == null) {
            return Result<bool>.FromError(ErrorType.ProductNotFound);
        }

        var fields = JsonDocument.Parse(JsonSerializer.Serialize(request.Value.Fields,
            CommonConstants.JsonOptionsWithStringEnum));

        var updateResult = await _dbContext.Products.Where(x => x.Id == request.Id && x.DeletedAt == null)
            .ExecuteUpdateAsync(x =>
                    x.SetProperty(p => p.ModifiedAt, modifiedAt)
                        .SetProperty(p => p.Name, request.Value.Name)
                        .SetProperty(p => p.CategoryId, request.Value.CategoryId)
                        .SetProperty(p => p.Fields, fields),
                cancellationToken: cancellationToken);

        if (updateResult == 0) {
            return Result<bool>.FromError(ErrorType.ProductNotFound);
        }

        var context = (request.Value, _validationContext.Value?.dict, _validationContext.Value?.category!);
        var productDto = context.Adapt<ProductDto>() with {
            Id = request.Id, ModifiedAt = modifiedAt, CreatedAt = currentProduct.CreatedAt
        };

        _dbContext.ProductOutbox.Add(new ProductOutbox() {
            OperationType = OperationType.Update,
            Data = JsonDocument.Parse(JsonSerializer.Serialize(productDto,
                CommonConstants.JsonOptionsWithStringEnum)),
        });

        await _dbContext.SaveChangesAsync(cancellationToken);
        await tr.CommitAsync(cancellationToken);

        return Result<bool>.FromResult(true);
    }
}