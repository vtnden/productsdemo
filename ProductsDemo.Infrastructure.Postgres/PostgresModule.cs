using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Npgsql;
using ProductsDemo.Domain.Constants;
using ProductsDemo.Domain.Enums;
using ProductsDemo.UseCases.Interfaces.DB;

namespace ProductsDemo.Infrastructure.Postgres;

/// <summary>
/// Вспомогательный модуль для инициализации базы
/// </summary>
public static class PostgresModule {
    public static void Init(IServiceCollection serviceCollection, string? dbName = null) {
        var dbHost = Environment.GetEnvironmentVariable(EnvConstants.DbHost)!;
        var dbPort = Environment.GetEnvironmentVariable(EnvConstants.DbPort)!;
        var dbUser = Environment.GetEnvironmentVariable(EnvConstants.DbUser)!;
        var dbPassword = Environment.GetEnvironmentVariable(EnvConstants.DbPassword) ?? "";
        dbName ??= Environment.GetEnvironmentVariable(EnvConstants.DbName)!;
        var environment = Environment.GetEnvironmentVariable(EnvConstants.AspNetCoreEnv)!;

        AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);

        var dbDataSource =
            new NpgsqlDataSourceBuilder(
                $"Host={dbHost};Port={dbPort};Database={dbName};Username={dbUser};Password={dbPassword}");

        dbDataSource.MapEnum<OperationType>();

        serviceCollection.AddPooledDbContextFactory<AppDbContext>(options => {
            if (environment == StringConstants.AspnetcoreDevelopment) {
                options.EnableSensitiveDataLogging();
            }

            options.UseNpgsql(dbDataSource.Build())
                .UseSnakeCaseNamingConvention();
        });

        serviceCollection.AddScoped<AppDbContext>(services =>
            services.GetRequiredService<IDbContextFactory<AppDbContext>>().CreateDbContext());

        serviceCollection.AddScoped<IRwDbContext>(services => services.GetRequiredService<AppDbContext>());
        serviceCollection.AddScoped<IDbContext>(services => services.GetRequiredService<IRwDbContext>());
    }
}