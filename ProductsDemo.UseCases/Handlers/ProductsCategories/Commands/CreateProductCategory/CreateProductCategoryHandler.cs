using System.Text.Json;
using Mapster;
using ProductsDemo.Domain.Common;
using ProductsDemo.Domain.Constants;
using ProductsDemo.Domain.Entities;
using ProductsDemo.Domain.Enums;
using ProductsDemo.UseCases.Handlers.ProductsCategories.Dto;
using ProductsDemo.UseCases.Interfaces.DB;
using ProductsDemo.UseCases.Interfaces.Handlers;

namespace ProductsDemo.UseCases.Handlers.ProductsCategories.Commands.CreateProductCategory;

public class CreateProductCategoryHandler: IRequestHandler<CreateProductCategoryCommand, bool> {
    private readonly IRwDbContext _dbContext;

    public CreateProductCategoryHandler(IRwDbContext dbContext) {
        _dbContext = dbContext;
    }

    public async Task<Result<bool>> Send(CreateProductCategoryCommand request, CancellationToken cancellationToken) {
        var newCategory = request.Adapt<ProductCategory>();

        var outBox = new ProductCategoryOutbox() {
            OperationType = OperationType.Create,
            Data = JsonDocument.Parse(JsonSerializer.Serialize(request.Adapt<ProductCategoryDto>(),
                CommonConstants.JsonOptionsWithStringEnum)),
        };

        _dbContext.ProductCategories.Add(newCategory);
        _dbContext.ProductCategoryOutbox.Add(outBox);
        await _dbContext.SaveChangesAsync(cancellationToken);

        return Result<bool>.FromResult(true);
    }
}