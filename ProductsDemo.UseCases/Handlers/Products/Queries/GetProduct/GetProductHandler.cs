using ProductsDemo.Domain.Common;
using ProductsDemo.UseCases.Handlers.Products.Dto;
using ProductsDemo.UseCases.Interfaces.Handlers;
using ProductsDemo.UseCases.Interfaces.Services;

namespace ProductsDemo.UseCases.Handlers.Products.Queries.GetProduct;

public class GetProductHandler: IQueryHandler<GetProductQuery, ProductDto?> {
    private readonly IProductService _service;

    public GetProductHandler(IProductService service) {
        _service = service;
    }

    public Task<Result<ProductDto?>> Send(GetProductQuery request,
        CancellationToken cancellationToken = default) {
        return _service.Get(request.Id);
    }
}