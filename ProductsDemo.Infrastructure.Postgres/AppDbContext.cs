using System.Transactions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Npgsql;
using ProductsDemo.Domain.Entities;
using ProductsDemo.Domain.Enums;
using ProductsDemo.UseCases.Interfaces.DB;

namespace ProductsDemo.Infrastructure.Postgres;

public class AppDbContext: DbContext, IRwDbContext {
    public DbSet<ProductCategory> ProductCategories { get; set; } = default!;
    public DbSet<Product> Products { set; get; } = default!;
    public DbSet<ProductOutbox> ProductOutbox { set; get; } = default!;
    public DbSet<ProductCategoryOutbox> ProductCategoryOutbox { set; get; } = default!;

    public AppDbContext(DbContextOptions<AppDbContext> options): base(options) {

    }

    public Task<IDbContextTransaction> CreateTransactionScope(IsolationLevel isolationLevel) =>
        Database.BeginTransactionAsync();

    public void Migrate() {
        Database.Migrate();
        if (Database.GetDbConnection() is not NpgsqlConnection npgsqlConnection)
            return;
        npgsqlConnection.Open();
        try {
            npgsqlConnection.ReloadTypes();
        }
        finally {
            npgsqlConnection.Close();
        }
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder) {
        base.OnModelCreating(modelBuilder);

        modelBuilder.HasPostgresEnum<OperationType>();

        modelBuilder.Entity<Product>(entity => { entity.Property(x => x.Fields).HasColumnType("jsonb"); });
        modelBuilder.Entity<ProductCategory>(entity => { entity.Property(x => x.Fields).HasColumnType("jsonb"); });

        modelBuilder.Entity<ProductOutbox>(entity => { entity.Property(x => x.Data).HasColumnType("jsonb"); });
        modelBuilder.Entity<ProductCategoryOutbox>(entity => { entity.Property(x => x.Data).HasColumnType("jsonb"); });
    }
}