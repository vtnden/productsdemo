using System.Reflection;
using Elastic.Clients.Elasticsearch;
using Elastic.Transport;
using Mapster;
using Mapster.Utils;
using Microsoft.Extensions.DependencyInjection;
using ProductsDemo.Domain.Constants;
using ProductsDemo.Infrastructure.Elasticsearch.Common;
using ProductsDemo.Infrastructure.Elasticsearch.Migrations;
using ProductsDemo.Infrastructure.Elasticsearch.Services;
using ProductsDemo.UseCases.Interfaces.Services;

namespace ProductsDemo.Infrastructure.Elasticsearch;

public static class ElasticsearchModule {

    public static void Init(IServiceCollection serviceCollection) {
        var host = Environment.GetEnvironmentVariable(EnvConstants.ElasticHost)!;
        var user = Environment.GetEnvironmentVariable(EnvConstants.ElasticUserName)!;
        var password = Environment.GetEnvironmentVariable(EnvConstants.ElasticUserPassword)!;
        var environment = Environment.GetEnvironmentVariable(EnvConstants.AspNetCoreEnv)!;

        var nodePool = new SingleNodePool(new Uri(host));

        var settings = new ElasticsearchClientSettings(nodePool,
                sourceSerializer: (_, settings) =>
                    new ElasticCustomJsonSerializer(settings))
            .Authentication(new BasicAuthentication(user, password));

        if (environment == StringConstants.AspnetcoreDevelopment) {
            settings.PrettyJson(); // Pretty json output
            settings.EnableDebugMode();
        }

        var client = new ElasticsearchClient(settings);


        Migration.Setup(settings, client);

        serviceCollection.AddSingleton(client);
        serviceCollection.AddSingleton<IProductService, ProductService>();
        serviceCollection.AddSingleton<IProductCategoryService, ProductCategoryService>();

        TypeAdapterConfig.GlobalSettings.ScanInheritedTypes(Assembly.GetExecutingAssembly());
    }

}