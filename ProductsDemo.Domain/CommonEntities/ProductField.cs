namespace ProductsDemo.Domain.CommonEntities;

public record ProductField(string FieldId, string? Value);