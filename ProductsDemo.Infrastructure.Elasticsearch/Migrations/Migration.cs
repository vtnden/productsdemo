using Elastic.Clients.Elasticsearch;
using ProductsDemo.Infrastructure.Elasticsearch.Migrations.Product;
using ProductsDemo.Infrastructure.Elasticsearch.Migrations.ProductCategory;

namespace ProductsDemo.Infrastructure.Elasticsearch.Migrations;

public static class Migration {
    public static void Setup(ElasticsearchClientSettings settings, ElasticsearchClient client) {
        ProductCategoryIndexMigration.CheckProductCategoryIndex(settings, client);
        ProductCategoryScriptsMigration.CheckProductCategoryScripts(client);
        ProductIndexMigration.CheckProductIndex(settings, client);
        ProductScriptsMigration.CheckProductScripts(client);
    }
}