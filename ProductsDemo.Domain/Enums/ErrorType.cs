namespace ProductsDemo.Domain.Enums;

/// <summary>
/// Custom error types
/// </summary>
public enum ErrorType {
    /// <summary>
    /// Where input fields has incorrect format or values.
    /// </summary>
    IncorrectInputData,

    /// <summary>
    /// When product category not found
    /// </summary>
    ProductCategoryNotFound,

    /// <summary>
    /// When some field not found
    /// </summary>
    FieldNotFound,

    /// <summary>
    /// Unknown error of domain. Use for something else.
    /// </summary>
    Unknown,

    /// <summary>
    /// When product not found
    /// </summary>
    ProductNotFound,
}