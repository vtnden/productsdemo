using Elastic.Clients.Elasticsearch.Core.Bulk;
using Elastic.Transport.Products.Elasticsearch;
using ProductsDemo.Domain.Common;
using ProductsDemo.Domain.Enums;

namespace ProductsDemo.Infrastructure.Elasticsearch.Extensions;

public static class ResponseExtensions {
    public static Result<TResult> ToResult<TResult>(this ElasticsearchResponse response, TResult result,
        ErrorType errorType = ErrorType.Unknown) {
        if (response.IsValidResponse) {
            return Result<TResult>.FromResult(result);
        }
        var error = response.ElasticsearchServerError?.Error;

        return Result<TResult>.FromError(errorType, new Dictionary<string, string[]>() {
            { error?.Type ?? "", new[] { $"{error?.Reason}, {error?.CausedBy}" } },
        });
    }

    public static Result<TResult> ToResult<TResult>(this ResponseItem response, TResult result,
        ErrorType errorType = ErrorType.Unknown) {
        if (response.IsValid) {
            return Result<TResult>.FromResult(result);
        }
        var error = response.Error;

        return Result<TResult>.FromError(errorType, new Dictionary<string, string[]>() {
            { error?.Type ?? "", new[] { $"{error?.Reason}, {error?.CausedBy}" } },
        });
    }
}