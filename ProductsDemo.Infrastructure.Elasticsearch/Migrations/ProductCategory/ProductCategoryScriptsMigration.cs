using System.Diagnostics;
using Elastic.Clients.Elasticsearch;
using ProductsDemo.Infrastructure.Elasticsearch.Constants;
using ProductsDemo.Infrastructure.Elasticsearch.Entities.ProductCategory;
using ProductsDemo.Infrastructure.Elasticsearch.Extensions;

namespace ProductsDemo.Infrastructure.Elasticsearch.Migrations.ProductCategory;

public static class ProductCategoryScriptsMigration {
    public static void CheckProductCategoryScripts(ElasticsearchClient client) {
        var response = CreateProductCategorySearchScript(client);
        Debug.Assert(response.IsSuccess());
    }

    private static PutScriptResponse CreateProductCategorySearchScript(ElasticsearchClient client) {
        var query = new SearchRequestDescriptor<ProductCategoryMultiLang>(IndexName.From<ProductCategoryMultiLang>())
            .Query(q =>
                q.Bool(b =>
                    b.Should(
                        s => s.Match(
                            m => m.Field(f => f.Id)
                                .Query($"{{{{{QueryConstants.ProductCategorySearchScriptTextParamName}}}}}")),
                        s => s.Match(
                            m => m.Field(f => f.Name)
                                .Query($"{{{{{QueryConstants.ProductCategorySearchScriptTextParamName}}}}}")
                                .Fuzziness(QueryConstants.FuzzinessOne)),
                        s => s.Match(
                            m => m.Field(f => f.NameEn)
                                .Query($"{{{{{QueryConstants.ProductCategorySearchScriptTextParamName}}}}}")
                                .Fuzziness(QueryConstants.FuzzinessOne)),
                        s => s.Nested(n => n.Path(p => p.Fields).Query(nq => nq.Bool(
                                    bb => bb.Should(
                                        bs => bs.Match(m =>
                                            m.Field(f => f.Fields!.First().Id).Query(
                                                $"{{{{{QueryConstants.ProductCategorySearchScriptTextParamName}}}}}")),
                                        bs => bs.Match(m =>
                                            m.Field(f => f.Fields!.First().FieldType)
                                                .Query(
                                                    $"{{{{{QueryConstants.ProductCategorySearchScriptTextParamName}}}}}")),
                                        bs => bs.Match(m =>
                                            m.Field(f => f.Fields!.First().Name)
                                                .Query(
                                                    $"{{{{{QueryConstants.ProductCategorySearchScriptTextParamName}}}}}")
                                                .Fuzziness(QueryConstants.FuzzinessOne)),
                                        bs => bs.Match(m =>
                                            m.Field(f => f.Fields!.First().NameEn)
                                                .Query(
                                                    $"{{{{{QueryConstants.ProductCategorySearchScriptTextParamName}}}}}")
                                                .Fuzziness(QueryConstants.FuzzinessOne))
                                    )
                                )
                            )
                        )
                    )
                )
            );

        return client.UploadScript(QueryConstants.ProductCategorySearchScriptName, query);

    }
}