using FluentAssertions;
using ProductsDemo.Domain.CommonEntities;
using ProductsDemo.Domain.Enums;
using ProductsDemo.UseCases.Handlers.ProductsCategories.Commands.UpdateProductCategory;
using ProductsDemo.UseCases.Handlers.ProductsCategories.Dto;
using ProductsDemo.UseCases.Validators.ProductCategory;

namespace ProductsDemo.UnitTests.CategoryProducts.Validators;

public class UpdateProductCategoryValidatorTests {
    [Fact]
    public async Task UpdateProductCategory_WithValidName_WithoutFields_ReturnTrue() {
        var validator = new UpdateProductCategoryValidator();
        var category = new UpdateProductCategoryCommand("1", new UpdateProductCategoryDto("Name", null));
        var result = await validator.ValidateAsync(category);

        result.IsValid.Should().BeTrue();
    }

    [Fact]
    public async Task UpdateProductCategory_WithInvalidName_WithoutFields_ReturnFalse() {
        var validator = new UpdateProductCategoryValidator();
        var category = new UpdateProductCategoryCommand("1", new UpdateProductCategoryDto("", null));
        var result = await validator.ValidateAsync(category);

        result.IsValid.Should().BeFalse();
    }

    [Fact]
    public async Task UpdateProductCategory_WithValidName_WithValidTextNameField_ReturnTrue() {
        var validator = new UpdateProductCategoryValidator();
        var category = new UpdateProductCategoryCommand("1", new UpdateProductCategoryDto("Name",
            new List<ProductCategoryField>() {
                new("f1", "Name", FieldType.Text, true, null),
            }));
        var result = await validator.ValidateAsync(category);

        result.IsValid.Should().BeTrue();
    }

    [Fact]
    public async Task UpdateProductCategory_WithValidName_WithInvalidNameField_ReturnFalse() {
        var validator = new UpdateProductCategoryValidator();
        var category = new UpdateProductCategoryCommand("1", new UpdateProductCategoryDto("Name",
            new List<ProductCategoryField>() {
                new("f1", "", FieldType.Text, true, null),
            }));
        var result = await validator.ValidateAsync(category);

        result.IsValid.Should().BeFalse();
    }

    [Fact]
    public async Task UpdateProductCategory_WithValidName_WithValidTextField_WithValidMaxProp_ReturnTrue() {
        var validator = new UpdateProductCategoryValidator();
        var category = new UpdateProductCategoryCommand("1", new UpdateProductCategoryDto("Name",
            new List<ProductCategoryField>() {
                new("f1", "Name", FieldType.Text, true, new List<FieldProp>() {
                    new(FieldPropType.Max, "10"),
                }),
            }));
        var result = await validator.ValidateAsync(category);

        result.IsValid.Should().BeTrue();
    }

    [Fact]
    public async Task UpdateProductCategory_WithValidName_WithValidField_WithInvalidMaxValueProp_ReturnFalse() {
        var validator = new UpdateProductCategoryValidator();
        var category = new UpdateProductCategoryCommand("1", new UpdateProductCategoryDto("Name",
            new List<ProductCategoryField>() {
                new("f1", "Name", FieldType.Text, true, new List<FieldProp>() {
                    new(FieldPropType.Max, "invalid"),
                }),
            }));
        var result = await validator.ValidateAsync(category);

        result.IsValid.Should().BeFalse();
    }

}