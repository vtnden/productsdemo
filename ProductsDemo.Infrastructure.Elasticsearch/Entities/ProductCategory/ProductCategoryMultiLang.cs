using ProductsDemo.Infrastructure.Elasticsearch.Entities.Base;

namespace ProductsDemo.Infrastructure.Elasticsearch.Entities.ProductCategory;

public class ProductCategoryMultiLang: ElasticEntityBase<string> {
    public required string Name { get; init; }
    public required string NameEn { get; init; }
    public IReadOnlyCollection<ProductCategoryMultiLangField>? Fields { get; init; }
}