namespace ProductsDemo.UseCases.Interfaces.Requests;

/// <summary>
/// Interface for all commands 
/// </summary>
public interface ICommandRequest: IRequest {

}