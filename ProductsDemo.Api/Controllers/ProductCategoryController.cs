using Microsoft.AspNetCore.Mvc;
using ProductsDemo.Api.Controllers.Base;
using ProductsDemo.Api.Extensions;
using ProductsDemo.Domain.Common;
using ProductsDemo.UseCases.Handlers.ProductsCategories.Commands.CreateProductCategory;
using ProductsDemo.UseCases.Handlers.ProductsCategories.Commands.DeleteProductCategory;
using ProductsDemo.UseCases.Handlers.ProductsCategories.Commands.UpdateProductCategory;
using ProductsDemo.UseCases.Handlers.ProductsCategories.Dto;
using ProductsDemo.UseCases.Handlers.ProductsCategories.Queries.GetProductCategory;
using ProductsDemo.UseCases.Handlers.ProductsCategories.Queries.SearchProductsCategories;
using ProductsDemo.UseCases.Interfaces.Handlers;

namespace ProductsDemo.Api.Controllers;

/// <summary>
/// Api for product category
/// </summary>
public class ProductCategoryController: ApiControllerBase {
    private readonly IRequestHandler<GetProductCategoryQuery, ProductCategoryDto?> _getHandler;
    private readonly IRequestHandler<CreateProductCategoryCommand, bool> _createHandler;
    private readonly IRequestHandler<UpdateProductCategoryCommand, bool> _updateHandler;
    private readonly IRequestHandler<DeleteProductCategoryCommand, bool> _deleteHandler;

    private readonly IRequestHandler<SearchProductsCategoriesQuery, IReadOnlyCollection<ProductCategoryDto>>
        _searchHandler;

    public ProductCategoryController(IRequestHandler<CreateProductCategoryCommand, bool> createHandler,
        IRequestHandler<GetProductCategoryQuery, ProductCategoryDto?> getHandler,
        IRequestHandler<SearchProductsCategoriesQuery, IReadOnlyCollection<ProductCategoryDto>> searchHandler,
        IRequestHandler<UpdateProductCategoryCommand, bool> updateHandler,
        IRequestHandler<DeleteProductCategoryCommand, bool> deleteHandler) {
        _createHandler = createHandler;
        _getHandler = getHandler;
        _searchHandler = searchHandler;
        _updateHandler = updateHandler;
        _deleteHandler = deleteHandler;
    }

    /// <summary>
    /// Get product category by id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Result<ProductCategoryDto?>.Ok))]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<ProductCategoryDto?>.Error))]
    [HttpGet("{id}")]
    public async Task<JsonResult> Get(string id) {
        var result = await _getHandler.Send(new GetProductCategoryQuery(id));
        return result.AsJsonResult();
    }

    /// <summary>
    /// Search products categories
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Result<IReadOnlyCollection<ProductCategoryDto>>.Ok))]
    [ProducesResponseType(StatusCodes.Status500InternalServerError,
        Type = typeof(Result<IReadOnlyCollection<ProductCategoryDto>>.Error))]
    [HttpGet("search")]
    public async Task<JsonResult> Search(string? text) {
        var result = await _searchHandler.Send(new SearchProductsCategoriesQuery(text));
        return result.AsJsonResult();
    }

    /// <summary>
    /// Create a new product category 
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Result<bool>.Ok))]
    [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<bool>.Error))]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<bool>.Error))]
    [HttpPost]
    public async Task<JsonResult> Create([FromBody] CreateProductCategoryDto value) {
        var result = await _createHandler.Send(new CreateProductCategoryCommand(value));
        return result.AsJsonResult();
    }

    /// <summary>
    /// Update product category 
    /// </summary>
    /// <param name="id"></param>
    /// <param name="value"></param>
    /// <returns></returns>
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Result<bool>.Ok))]
    [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<bool>.Error))]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<bool>.Error))]
    [HttpPut("{id}")]
    public async Task<JsonResult> Update([FromRoute] string id, [FromBody] UpdateProductCategoryDto value) {
        var result = await _updateHandler.Send(new UpdateProductCategoryCommand(id, value));
        return result.AsJsonResult();
    }

    /// <summary>
    /// Delete product category 
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Result<bool>.Ok))]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<bool>.Error))]
    [HttpDelete("{id}")]
    public async Task<JsonResult> Delete([FromRoute] string id) {
        var result = await _deleteHandler.Send(new DeleteProductCategoryCommand(id));
        return result.AsJsonResult();
    }
}