using ProductsDemo.Domain.Enums;

namespace ProductsDemo.Domain.CommonEntities;

public record FieldProp(FieldPropType FieldPropType, string? Value);