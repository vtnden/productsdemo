namespace ProductsDemo.Domain.Enums;

public enum OperationType {
    Create,
    Update,
    Delete
}