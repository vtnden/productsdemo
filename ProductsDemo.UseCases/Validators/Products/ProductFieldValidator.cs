using System.ComponentModel.DataAnnotations.Schema;
using FluentValidation;
using ProductsDemo.Domain.CommonEntities;
using ProductsDemo.Domain.Enums;

namespace ProductsDemo.UseCases.Validators.Products;

/// <summary>
/// Validator for each product field
/// </summary>
[NotMapped]
public class ProductFieldValidator: AbstractValidator<ProductField> {
    public ProductFieldValidator(ProductCategoryField? field) {
        var props = field?.Props?.ToDictionary(x => x.FieldPropType);

        RuleFor(x => x.Value).Must(_ => field != null)
            .WithErrorCode(ErrorType.FieldNotFound.ToString());

        RuleFor(x => x.Value).NotNull()
            .DependentRules(() => { RuleFor(x => x.Value).Must(_ => field?.Required == true); });

        if (props?.TryGetValue(FieldPropType.Min, out var prop) == true) {
            if (int.TryParse(prop.Value, out var length)) {
                RuleFor(x => x.Value).MinimumLength(length);
            }
        }

        if (props?.TryGetValue(FieldPropType.Max, out var maxProp) == true) {
            if (int.TryParse(maxProp.Value, out var length)) {
                RuleFor(x => x.Value).MaximumLength(length);
            }
        }
    }
}