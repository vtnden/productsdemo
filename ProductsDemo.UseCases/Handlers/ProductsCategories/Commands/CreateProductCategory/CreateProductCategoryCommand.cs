using ProductsDemo.Domain.CommonEntities;
using ProductsDemo.UseCases.Handlers.ProductsCategories.Dto;
using ProductsDemo.UseCases.Interfaces.Requests.ProductCategory;

namespace ProductsDemo.UseCases.Handlers.ProductsCategories.Commands.CreateProductCategory;

public record CreateProductCategoryCommand(CreateProductCategoryDto Value): IEditProductCategoryCommand {
    public string Id => Value.Id;
    public string Name => Value.Name;
    public IReadOnlyCollection<ProductCategoryField>? Fields => Value.Fields;
}