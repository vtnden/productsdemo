using System.Text.Json;
using System.Transactions;
using Microsoft.EntityFrameworkCore;
using ProductsDemo.Domain.Common;
using ProductsDemo.Domain.Constants;
using ProductsDemo.Domain.Entities;
using ProductsDemo.Domain.Enums;
using ProductsDemo.UseCases.Interfaces.DB;
using ProductsDemo.UseCases.Interfaces.Handlers;

namespace ProductsDemo.UseCases.Handlers.ProductsCategories.Commands.DeleteProductCategory;

public class DeleteProductCategoryHandler: IRequestHandler<DeleteProductCategoryCommand, bool> {
    private readonly IRwDbContext _dbContext;

    public DeleteProductCategoryHandler(IRwDbContext dbContext) {
        _dbContext = dbContext;
    }

    public async Task<Result<bool>> Send(DeleteProductCategoryCommand request, CancellationToken cancellationToken) {
        await using var tr = await _dbContext.CreateTransactionScope(IsolationLevel.ReadCommitted);

        var count = await _dbContext.ProductCategories.Where(x => x.Id == request.Id && x.DeletedAt == null)
            .ExecuteUpdateAsync(
                x =>
                    x.SetProperty(p => p.DeletedAt, DateTimeOffset.Now),
                cancellationToken: cancellationToken);

        if (count == 0) {
            return Result<bool>.FromError(ErrorType.ProductCategoryNotFound);
        }

        var outBox = new ProductCategoryOutbox() {
            OperationType = OperationType.Delete,
            Data = JsonDocument.Parse(JsonSerializer.Serialize(request.Id,
                CommonConstants.JsonOptionsWithStringEnum)),
        };
        _dbContext.ProductCategoryOutbox.Add(outBox);

        await _dbContext.SaveChangesAsync(cancellationToken);
        await tr.CommitAsync(cancellationToken);

        return Result<bool>.FromResult(true);
    }
}