using Mapster;
using ProductsDemo.Infrastructure.Elasticsearch.Entities.Product;
using ProductsDemo.UseCases.Handlers.Products.Dto;

namespace ProductsDemo.Infrastructure.Elasticsearch.Mapping.Product;

public class ProductToMultiLang: IMapFrom<ProductDto> {
    public void ConfigureMapping(TypeAdapterConfig config) {
        config
            .NewConfig<ProductDto, ProductMultiLang>()
            .Map(x => x.NameEn, source => source.Name)
            .Map(x => x.CategoryEn, source => source.Category)
            .Map(x => x.Fields, source => source.Fields!.Select(x => x.Adapt<ProductMultiLangField>()).ToList())
            .IgnoreNullValues(true);
    }
}