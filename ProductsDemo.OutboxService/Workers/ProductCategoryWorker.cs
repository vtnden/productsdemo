using System.ComponentModel;
using System.Text.Json;
using System.Transactions;
using Microsoft.EntityFrameworkCore;
using ProductsDemo.Domain.Common;
using ProductsDemo.Domain.Constants;
using ProductsDemo.Domain.Entities;
using ProductsDemo.Domain.Enums;
using ProductsDemo.UseCases.Handlers.ProductsCategories.Dto;
using ProductsDemo.UseCases.Interfaces.DB;
using ProductsDemo.UseCases.Interfaces.Services;

namespace ProductsDemo.OutboxService.Workers;

public class ProductCategoryWorker: BackgroundService {
    private readonly IProductCategoryService _productCategoryService;
    private readonly IServiceProvider _serviceProvider;
    private readonly ILogger<ProductCategoryWorker> _logger;
    private const int BatchSize = 12;

    public ProductCategoryWorker(ILogger<ProductCategoryWorker> logger, IServiceProvider serviceProvider,
        IProductCategoryService productCategoryService) {
        _logger = logger;
        _serviceProvider = serviceProvider;
        _productCategoryService = productCategoryService;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken) {
        using var scope = _serviceProvider.CreateScope();
        var dbContext = scope.ServiceProvider.GetRequiredService<IRwDbContext>();
        var tableName = dbContext.ProductCategoryOutbox.EntityType.GetTableName();
        var deletedAtName = dbContext.ProductCategoryOutbox.EntityType
            .GetProperty(nameof(ProductCategoryOutbox.DeletedAt))
            .GetColumnName();

        _logger.LogInformation("ProductCategoryWorker running");

        while (!stoppingToken.IsCancellationRequested) {
            await using var tr = await dbContext.CreateTransactionScope(IsolationLevel.ReadCommitted);
            var categories =
                await dbContext.ProductCategoryOutbox
                    .FromSqlRaw($"""
                                 select * from {tableName} where {deletedAtName} IS NULL
                                 limit {BatchSize} for update
                                 """)
                    .ToListAsync(
                        cancellationToken: stoppingToken);

            if (categories.Count == 0) {
                await Task.Delay(1000, stoppingToken);
                continue;
            }
            _logger.LogInformation("Found product categories: {CategoriesCount}", categories.Count);

            var results = await Process(categories);
            var resultId = results.Select(x => x.Id).ToArray();
            var currentDate = DateTimeOffset.Now;

            await dbContext.ProductCategoryOutbox.Where(x => resultId.Contains(x.Id))
                .ExecuteUpdateAsync(x => x.SetProperty(p => p.DeletedAt, currentDate),
                    cancellationToken: stoppingToken);

            await tr.CommitAsync(stoppingToken);

            if (categories.Count == results.Count) {
                _logger.LogInformation(
                    "All operations for products categories has been completed");
            }
            else {
                _logger.LogWarning(
                    "({OperationsCount}/{ResultCount}) operations for products categories has been completed",
                    categories.Count, results.Count);
            }

        }
    }

    private async Task<List<ProductCategoryOutbox>> Process(IReadOnlyList<ProductCategoryOutbox> categories) {
        var serviceResults = new[] {
            Task.FromResult<IReadOnlyCollection<Result<bool>>>(new List<Result<bool>>()),
            Task.FromResult<IReadOnlyCollection<Result<bool>>>(new List<Result<bool>>()),
            Task.FromResult<IReadOnlyCollection<Result<bool>>>(new List<Result<bool>>()),
        };
        var groupedCategories = categories.GroupBy(x => x.OperationType);

        foreach (var group in groupedCategories) {
            switch (group.Key) {
                case OperationType.Create: {
                    var data = group.Select(groupedCategory =>
                        groupedCategory.Data.RootElement.Deserialize<ProductCategoryDto>(CommonConstants
                            .JsonOptionsWithStringEnum)!).ToList();
                    serviceResults[0] = _productCategoryService.Create(data);
                    break;
                }
                case OperationType.Update: {
                    var data = group.Select(groupedCategory =>
                        groupedCategory.Data.RootElement.Deserialize<ProductCategoryDto>(CommonConstants
                            .JsonOptionsWithStringEnum)!).ToList();
                    serviceResults[1] = _productCategoryService.Update(data);
                    break;
                }
                case OperationType.Delete: {
                    var data = group.Select(groupedCategory =>
                        groupedCategory.Data.RootElement.Deserialize<string>(CommonConstants
                            .JsonOptionsWithStringEnum)!).ToList();
                    serviceResults[2] = _productCategoryService.Delete(data);
                    break;
                }
                default:
                    throw new InvalidEnumArgumentException();
            }
        }

        var taskResults = await Task.WhenAll(serviceResults);
        var taskListResults = taskResults.SelectMany(x => x).ToArray();
        var finalResults = new List<ProductCategoryOutbox>();

        for (var i = 0; i < taskListResults.Length; i++) {
            if (taskListResults[i] is Result<bool>.Ok) {
                finalResults.Add(categories[i]);
                _logger.LogInformation("Operation {OperationType} for product category id ({Data}) has been completed",
                    categories[i].OperationType, categories[i].Id);
            }
            else {
                var error = taskListResults[i] as Result<bool>.Error;
                _logger.LogError(
                    "Operation {OperationType} for product category id ({CategoryId}) failed. ErrorType: {ErrorType}",
                    categories[i].OperationType, categories[i].Id, error?.Type);
            }
        }

        return finalResults;
    }
}