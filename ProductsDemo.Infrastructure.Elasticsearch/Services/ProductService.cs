using Elastic.Clients.Elasticsearch;
using Mapster;
using ProductsDemo.Domain.Common;
using ProductsDemo.Infrastructure.Elasticsearch.Constants;
using ProductsDemo.Infrastructure.Elasticsearch.Entities.Product;
using ProductsDemo.Infrastructure.Elasticsearch.Extensions;
using ProductsDemo.UseCases.Handlers.Products.Dto;
using ProductsDemo.UseCases.Interfaces.Services;

namespace ProductsDemo.Infrastructure.Elasticsearch.Services;

public class ProductService: IProductService {
    private readonly ElasticsearchClient _client;

    public ProductService(ElasticsearchClient client) {
        _client = client;
    }

    public async Task<Result<ProductDto?>> Get(int id) {
        var response = await _client.GetAsync<ProductMultiLang>(id);
        return response.ToResult(response.Source.Adapt<ProductDto?>());
    }

    public async Task<IReadOnlyCollection<Result<bool>>> Create(IReadOnlyCollection<ProductDto> products) {
        var response = await _client.BulkAsync(b => {
            foreach (var product in products) {
                b.Index(product.Adapt<ProductMultiLang>());
            }
        });

        return response.Items.Select(x => x.ToResult(true)).ToList();
    }

    public async Task<Result<IReadOnlyCollection<ProductDto>>> Search(string? text) {
        if (string.IsNullOrEmpty(text)) {
            var response = await _client.SearchAsync<ProductMultiLang>(q => q.Query(q => q.MatchAll()));
            return response.ToResult(response.Documents.Adapt<IReadOnlyCollection<ProductDto>>());
        }

        var searchResponse = await _client.SearchTemplateAsync<ProductMultiLang>(
            st =>
                st.Indices(IndexName.From<ProductMultiLang>())
                    .Id(QueryConstants.ProductCategorySearchScriptName)
                    .Params(p => p.Add(QueryConstants.ProductCategorySearchScriptTextParamName, text))
        );

        return searchResponse.ToResult(searchResponse.Hits.Hits.Select(x => x.Source)
            .Adapt<IReadOnlyCollection<ProductDto>>());
    }

    public async Task<IReadOnlyCollection<Result<bool>>> Update(IReadOnlyCollection<ProductDto> products) {
        var response = await _client.BulkAsync(b => {
            foreach (var product in products) {
                b.Update<ProductMultiLang>(u =>
                    u.Index<ProductMultiLang>().Id(new Id(product.Id))
                        .Doc(product.Adapt<ProductMultiLang>()));
            }
        });

        return response.Items.Select(x => x.ToResult(true)).ToList();
    }

    public async Task<IReadOnlyCollection<Result<bool>>> Delete(IReadOnlyCollection<int> productIds) {
        var response = await _client.BulkAsync(b => {
            foreach (var id in productIds) {
                b.Delete<ProductMultiLang>(x => x.Id(id).Index<ProductMultiLang>());
            }
        });

        return response.Items.Select(x => x.ToResult(true)).ToList();
    }

}