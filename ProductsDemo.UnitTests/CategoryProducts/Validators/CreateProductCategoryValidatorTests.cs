using FluentAssertions;
using ProductsDemo.Domain.CommonEntities;
using ProductsDemo.Domain.Enums;
using ProductsDemo.UseCases.Handlers.ProductsCategories.Commands.CreateProductCategory;
using ProductsDemo.UseCases.Handlers.ProductsCategories.Dto;
using ProductsDemo.UseCases.Validators.ProductCategory;

namespace ProductsDemo.UnitTests.CategoryProducts.Validators;

public class CreateProductCategoryValidatorTests {
    [Fact]
    public async Task CreateProductCategory_WithValidName_WithoutFields_ReturnTrue() {
        var validator = new CreateProductCategoryValidator();
        var category = new CreateProductCategoryCommand(new CreateProductCategoryDto("1", "Name", null));
        var result = await validator.ValidateAsync(category);

        result.IsValid.Should().BeTrue();
    }

    [Fact]
    public async Task CreateProductCategory_WithInvalidName_WithoutFields_ReturnFalse() {
        var validator = new CreateProductCategoryValidator();
        var category = new CreateProductCategoryCommand(new CreateProductCategoryDto("1", "", null));
        var result = await validator.ValidateAsync(category);

        result.IsValid.Should().BeFalse();
    }

    [Fact]
    public async Task CreateProductCategory_WithValidName_WithValidTextNameField_ReturnTrue() {
        var validator = new CreateProductCategoryValidator();
        var category = new CreateProductCategoryCommand(new CreateProductCategoryDto("1", "Name",
            new List<ProductCategoryField>() {
                new("f1", "Name", FieldType.Text, true, null),
            }));
        var result = await validator.ValidateAsync(category);

        result.IsValid.Should().BeTrue();
    }

    [Fact]
    public async Task CreateProductCategory_WithValidName_WithInvalidNameField_ReturnFalse() {
        var validator = new CreateProductCategoryValidator();
        var category = new CreateProductCategoryCommand(new CreateProductCategoryDto("1", "Name",
            new List<ProductCategoryField>() {
                new("f1", "", FieldType.Text, true, null),
            }));
        var result = await validator.ValidateAsync(category);

        result.IsValid.Should().BeFalse();
    }

    [Fact]
    public async Task CreateProductCategory_WithValidName_WithValidTextField_WithValidMaxProp_ReturnTrue() {
        var validator = new CreateProductCategoryValidator();
        var category = new CreateProductCategoryCommand(new CreateProductCategoryDto("1", "Name",
            new List<ProductCategoryField>() {
                new("f1", "Name", FieldType.Text, true, new List<FieldProp>() {
                    new(FieldPropType.Max, "10"),
                }),
            }));
        var result = await validator.ValidateAsync(category);

        result.IsValid.Should().BeTrue();
    }

    [Fact]
    public async Task CreateProductCategory_WithValidName_WithValidField_WithInvalidMaxValueProp_ReturnFalse() {
        var validator = new CreateProductCategoryValidator();
        var category = new CreateProductCategoryCommand(new CreateProductCategoryDto("1", "Name",
            new List<ProductCategoryField>() {
                new("f1", "Name", FieldType.Text, true, new List<FieldProp>() {
                    new(FieldPropType.Max, "invalid"),
                }),
            }));
        var result = await validator.ValidateAsync(category);

        result.IsValid.Should().BeFalse();
    }

}