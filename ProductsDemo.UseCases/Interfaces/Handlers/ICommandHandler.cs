using ProductsDemo.UseCases.Interfaces.Requests;

namespace ProductsDemo.UseCases.Interfaces.Handlers;

/// <summary>
/// Interface for all commands handlers
/// </summary>
/// <typeparam name="TRequest"></typeparam>
/// <typeparam name="TResult"></typeparam>
public interface ICommandHandler<in TRequest, TResult>: IRequestHandler<TRequest, TResult>
    where TRequest : ICommandRequest {

}