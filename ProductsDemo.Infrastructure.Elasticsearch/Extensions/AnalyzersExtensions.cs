using Elastic.Clients.Elasticsearch.Analysis;
using Elastic.Clients.Elasticsearch.IndexManagement;
using Elastic.Clients.Elasticsearch.Mapping;

namespace ProductsDemo.Infrastructure.Elasticsearch.Extensions;

public static class AnalyzersExtensions {

    private static string GetKeyWordMarker(this Language language) => language switch {
        Language.Russian => "пример",
        Language.English => "example",
        _ => throw new ArgumentOutOfRangeException(nameof(language), language, null),
    };

    public static IndexSettingsDescriptor<TEntity> AddAnalyzers<TEntity>(this IndexSettingsDescriptor<TEntity> settings,
        params Language[] languages) {
        return settings.Analysis(a => a.Analyzers(aa => {
                foreach (var lang in languages) {
                    var langName = lang.ToString().ToLower();
                    aa.Custom($"{langName}_analyzer", c => c.Tokenizer("standard")
                        .Filter(new List<string>() {
                            "lowercase",
                            $"{langName}_stop",
                            $"{langName}_stemmer",
                            $"{langName}_keywords",
                        }));
                }
            }).TokenFilters(t => {
                foreach (var lang in languages) {
                    var langName = lang.ToString().ToLower();
                    t.Stop($"{langName}_stop", ss => ss.Stopwords(new[] { $"_{langName}_" }))
                        .KeywordMarker($"{langName}_keywords", kw => kw.Keywords(new List<string>() {
                            $"{lang.GetKeyWordMarker()}",
                        }))
                        .Stemmer($"{langName}_stemmer", st => st.Language(langName));
                }
            })
        );
    }

    public static TextPropertyDescriptor<TEntity> UseAnalyzer<TEntity>(this TextPropertyDescriptor<TEntity> settings,
        Language lang) {
        var langName = lang.ToString().ToLower();
        return settings.Analyzer($"{langName}_analyzer");
    }
}