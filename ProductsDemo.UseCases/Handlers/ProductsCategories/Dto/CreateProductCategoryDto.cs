using ProductsDemo.Domain.CommonEntities;

namespace ProductsDemo.UseCases.Handlers.ProductsCategories.Dto;

public record CreateProductCategoryDto(string Id, string Name, IReadOnlyCollection<ProductCategoryField>? Fields);