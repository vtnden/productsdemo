using System.Text.Json;
using System.Text.Json.Serialization;

namespace ProductsDemo.Domain.Constants;

public static class CommonConstants {
    public static readonly JsonSerializerOptions JsonOptionsWithStringEnum = new() {
        PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
        Converters = { new JsonStringEnumConverter() },
    };
}