using ProductsDemo.Domain.Enums;

namespace ProductsDemo.Domain.Entities.Base;

/// <summary>
/// Outbox table for export entities
/// </summary>
public class OutboxBase<TData>: EntityBase<int> {
    public OperationType OperationType { get; set; }
    public TData Data { get; set; } = default!;
}