using Elastic.Clients.Elasticsearch;
using Elastic.Transport.Extensions;

namespace ProductsDemo.Infrastructure.Elasticsearch.Extensions;

public static class ClientExtensions {
    public static PutScriptResponse UploadScript<TQuery>(this ElasticsearchClient client, string name, TQuery query) {
        var script = client.RequestResponseSerializer.SerializeToString(query);
        return client.PutScript(name,
            s => s.Script(descriptor => descriptor.Source(script).Language(ScriptLanguage.Mustache)));
    }
}