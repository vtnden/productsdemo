using ProductsDemo.UseCases.Interfaces.Requests;

namespace ProductsDemo.UseCases.Handlers.Products.Queries.SearchProducts;

public record SearchProductsQuery(string? Text): IQueryRequest;