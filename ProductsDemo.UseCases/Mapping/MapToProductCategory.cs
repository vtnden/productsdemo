using System.Text.Json;
using Mapster;
using ProductsDemo.Domain.Constants;
using ProductsDemo.Domain.Entities;
using ProductsDemo.UseCases.Handlers.ProductsCategories.Commands.CreateProductCategory;
using ProductsDemo.UseCases.Interfaces.Requests.ProductCategory;

namespace ProductsDemo.UseCases.Mapping;

/// <summary>
/// Mapping for CreateProductCategoryCommand -> ProductCategory
/// </summary>
public class MapToCategoryProduct: IMapFrom<IEditProductCategoryCommand> {
    public void ConfigureMapping(TypeAdapterConfig config) {
        config
            .NewConfig<CreateProductCategoryCommand, ProductCategory>()
            .Map(x => x.Id, source => source.Id)
            .Map(x => x.Name, source => source.Name)
            .Map(x => x.Fields,
                source => JsonDocument.Parse(JsonSerializer.Serialize(source.Fields,
                    CommonConstants.JsonOptionsWithStringEnum), new JsonDocumentOptions()))
            .IgnoreNullValues(true);
    }
}