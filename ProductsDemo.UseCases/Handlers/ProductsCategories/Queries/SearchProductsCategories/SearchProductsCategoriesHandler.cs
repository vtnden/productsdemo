using ProductsDemo.Domain.Common;
using ProductsDemo.UseCases.Handlers.ProductsCategories.Dto;
using ProductsDemo.UseCases.Interfaces.Handlers;
using ProductsDemo.UseCases.Interfaces.Services;

namespace ProductsDemo.UseCases.Handlers.ProductsCategories.Queries.SearchProductsCategories;

public class
    SearchProductsCategoriesHandler: IQueryHandler<SearchProductsCategoriesQuery,
        IReadOnlyCollection<ProductCategoryDto>?> {
    private readonly IProductCategoryService _service;

    public SearchProductsCategoriesHandler(IProductCategoryService service) {
        _service = service;
    }

    public Task<Result<IReadOnlyCollection<ProductCategoryDto>?>> Send(SearchProductsCategoriesQuery request,
        CancellationToken cancellationToken = default) {
        return _service.Search(request.Text);
    }
}