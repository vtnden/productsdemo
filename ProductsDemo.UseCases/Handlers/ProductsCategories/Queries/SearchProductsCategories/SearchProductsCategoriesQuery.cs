using ProductsDemo.UseCases.Interfaces.Requests;

namespace ProductsDemo.UseCases.Handlers.ProductsCategories.Queries.SearchProductsCategories;

public record SearchProductsCategoriesQuery(string? Text): IQueryRequest;