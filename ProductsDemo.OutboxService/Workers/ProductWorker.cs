using System.ComponentModel;
using System.Text.Json;
using System.Transactions;
using Microsoft.EntityFrameworkCore;
using ProductsDemo.Domain.Common;
using ProductsDemo.Domain.Constants;
using ProductsDemo.Domain.Entities;
using ProductsDemo.Domain.Enums;
using ProductsDemo.UseCases.Handlers.Products.Dto;
using ProductsDemo.UseCases.Interfaces.DB;
using ProductsDemo.UseCases.Interfaces.Services;

namespace ProductsDemo.OutboxService.Workers;

public class ProductWorker: BackgroundService {
    private readonly IProductService _productService;
    private readonly IServiceProvider _serviceProvider;
    private readonly ILogger<ProductWorker> _logger;
    private const int BatchSize = 12;

    public ProductWorker(ILogger<ProductWorker> logger, IServiceProvider serviceProvider,
        IProductService productService) {
        _logger = logger;
        _serviceProvider = serviceProvider;
        _productService = productService;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken) {
        using var scope = _serviceProvider.CreateScope();
        var dbContext = scope.ServiceProvider.GetRequiredService<IRwDbContext>();
        var tableName = dbContext.ProductOutbox.EntityType.GetTableName();
        var deletedAtName = dbContext.ProductOutbox.EntityType
            .GetProperty(nameof(ProductOutbox.DeletedAt))
            .GetColumnName();

        _logger.LogInformation("ProductWorker running");

        while (!stoppingToken.IsCancellationRequested) {
            await using var tr = await dbContext.CreateTransactionScope(IsolationLevel.ReadCommitted);
            var products =
                await dbContext.ProductOutbox
                    .FromSqlRaw($"""
                                 select * from {tableName} where {deletedAtName} IS NULL
                                 limit {BatchSize} for update
                                 """)
                    .ToListAsync(
                        cancellationToken: stoppingToken);

            if (products.Count == 0) {
                await Task.Delay(1000, stoppingToken);
                continue;
            }
            _logger.LogInformation("Found products: {ProductsCount}", products.Count);

            var results = await Process(products);
            var resultId = results.Select(x => x.Id).ToArray();
            var currentDate = DateTimeOffset.Now;

            await dbContext.ProductOutbox.Where(x => resultId.Contains(x.Id))
                .ExecuteUpdateAsync(x => x.SetProperty(p => p.DeletedAt, currentDate),
                    cancellationToken: stoppingToken);

            await tr.CommitAsync(stoppingToken);

            if (products.Count == results.Count) {
                _logger.LogInformation(
                    "All operations for products has been completed");
            }
            else {
                _logger.LogWarning(
                    "({OperationsCount}/{ResultCount}) for products has been completed",
                    products.Count, results.Count);
            }
        }
    }

    private async Task<List<ProductOutbox>> Process(IReadOnlyList<ProductOutbox> products) {
        var serviceResults = new[] {
            Task.FromResult<IReadOnlyCollection<Result<bool>>>(new List<Result<bool>>()),
            Task.FromResult<IReadOnlyCollection<Result<bool>>>(new List<Result<bool>>()),
            Task.FromResult<IReadOnlyCollection<Result<bool>>>(new List<Result<bool>>()),
        };
        var groupedCategories = products.GroupBy(x => x.OperationType);

        foreach (var group in groupedCategories) {
            switch (group.Key) {
                case OperationType.Create: {
                    var data = group.Select(groupedCategory =>
                        groupedCategory.Data.RootElement.Deserialize<ProductDto>(CommonConstants
                            .JsonOptionsWithStringEnum)!).ToList();
                    serviceResults[0] = _productService.Create(data);
                    break;
                }
                case OperationType.Update: {
                    var data = group.Select(groupedCategory =>
                        groupedCategory.Data.RootElement.Deserialize<ProductDto>(CommonConstants
                            .JsonOptionsWithStringEnum)!).ToList();
                    serviceResults[1] = _productService.Update(data);
                    break;
                }
                case OperationType.Delete: {
                    var data = group.Select(groupedCategory =>
                        groupedCategory.Data.RootElement.Deserialize<int>(CommonConstants
                            .JsonOptionsWithStringEnum)!).ToList();
                    serviceResults[2] = _productService.Delete(data);
                    break;
                }
                default:
                    throw new InvalidEnumArgumentException();
            }
        }

        var taskResults = await Task.WhenAll(serviceResults);
        var taskListResults = taskResults.SelectMany(x => x).ToArray();
        var finalResults = new List<ProductOutbox>();

        for (var i = 0; i < taskListResults.Length; i++) {
            if (taskListResults[i] is Result<bool>.Ok) {
                finalResults.Add(products[i]);
                _logger.LogInformation("Operation {OperationType} for product category id ({Data}) has been completed",
                    products[i].OperationType, products[i].Id);
            }
            else {
                var error = taskListResults[i] as Result<bool>.Error;
                _logger.LogError(
                    "Operation {OperationType} for product category id ({CategoryId}) failed. ErrorType: {ErrorType}",
                    products[i].OperationType, products[i].Id, error?.Type);
            }
        }


        return finalResults;
    }
}