using ProductsDemo.UseCases.Handlers.Products.Dto;

namespace ProductsDemo.UseCases.Interfaces.Requests.Products;

public interface IEditProductCommand: ICommandRequest {
    CreateProductDto Value { get; }
}