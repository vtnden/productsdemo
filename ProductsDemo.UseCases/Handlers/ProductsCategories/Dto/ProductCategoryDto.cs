using ProductsDemo.Domain.CommonEntities;

namespace ProductsDemo.UseCases.Handlers.ProductsCategories.Dto;

public record ProductCategoryDto(string Id, string Name, IReadOnlyCollection<ProductCategoryField>? Fields,
    DateTimeOffset CreatedAt, DateTimeOffset ModifiedAt);