using System.Text.Json;
using System.Text.Json.Serialization;
using NLog;
using NLog.Extensions.Logging;
using NLog.Web;
using ProductsDemo.Api.Swagger;
using ProductsDemo.Domain.Constants;
using ProductsDemo.Infrastructure.Elasticsearch;
using ProductsDemo.Infrastructure.Postgres;
using ProductsDemo.UseCases;
using ProductsDemo.UseCases.Interfaces.DB;

var currentEnv = Environment.GetEnvironmentVariable(EnvConstants.AspNetCoreEnv)!;

#region Init Nlog

var config = new ConfigurationBuilder()
    .SetBasePath(Directory.GetCurrentDirectory())
    .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
    .AddJsonFile($"appsettings.{currentEnv}.json", optional: true)
    .AddEnvironmentVariables().Build();

var logger = LogManager.Setup()
    .LoadConfigurationFromSection(config)
    .GetCurrentClassLogger();

#endregion

try {
    var builder = WebApplication.CreateBuilder(args);
    builder.Configuration
        .AddConfiguration(config);

    builder.Logging.ClearProviders();
    builder.Host.UseNLog();

    builder.Services.AddControllers().AddJsonOptions(options => {
        options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
        options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
    });

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
    builder.Services.AddEndpointsApiExplorer();

    #region Swagger

    builder.Services.AddSwaggerGen(options => {
        var assemblies = AppDomain.CurrentDomain.GetAssemblies()
            .Where(x => x.GetName().Name?.StartsWith(StringConstants.SolutionName) ?? false);

        foreach (var assembly in assemblies) {
            var xmlPath = Path.Combine(AppContext.BaseDirectory, $"{assembly.GetName().Name}.xml");
            if (File.Exists(xmlPath)) {
                options.IncludeXmlComments(xmlPath);
            }
        }

        options.SchemaFilter<EnumSchemaFilter>();
    });

    #endregion


    // Connecting postgres
    PostgresModule.Init(builder.Services);

    // Connecting application handlers
    UseCasesModule.Init(builder.Services);

    // Elasticsearch
    ElasticsearchModule.Init(builder.Services);

    var app = builder.Build();

    // Migration
    {
        using var scope = app.Services.CreateScope();
        var db = scope.ServiceProvider.GetRequiredService<IRwDbContext>();
        db.Migrate();
    }


    app.UseSwagger();
    app.UseSwaggerUI();

//app.UseHttpsRedirection();

    app.UseAuthorization();

    app.MapControllers();

    app.Run();
}
catch (Exception e) {
    logger.Error(e, "Stopped program because of exception");
    throw;
}
finally {
    LogManager.Shutdown();
}