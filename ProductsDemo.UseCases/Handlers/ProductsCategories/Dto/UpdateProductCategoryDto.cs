using ProductsDemo.Domain.CommonEntities;

namespace ProductsDemo.UseCases.Handlers.ProductsCategories.Dto;

public record UpdateProductCategoryDto(string Name, IReadOnlyCollection<ProductCategoryField>? Fields);