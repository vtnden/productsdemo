using Elastic.Clients.Elasticsearch;

namespace ProductsDemo.Infrastructure.Elasticsearch.Constants;

public static class QueryConstants {
    public static readonly Fuzziness FuzzinessOne = new(1);

    #region ProductCategory

    public const string ProductCategorySearchScriptName = "product-category-search-script";
    public const string ProductCategorySearchScriptTextParamName = "text";

    #endregion

    #region Product

    public const string ProductSearchScriptName = "product-search-script";
    public const string ProductSearchScriptTextParamName = "text";

    #endregion

}