using ProductsDemo.Domain.Enums;

namespace ProductsDemo.Domain.Common;

/// <summary>
/// Wrapper for application results
/// </summary>
/// <typeparam name="TValue">Result type</typeparam>
public abstract class Result<TValue> {

    public class Ok: Result<TValue> {
        public TValue Value { get; set; } = default!;
    }

    public class Error: Result<TValue> {
        public ErrorType Type { get; set; }
        public Dictionary<string, string[]>? Errors { get; set; }
    }

    public static Ok FromResult(TValue result) => new() { Value = result };

    public static Error FromError(ErrorType type, Dictionary<string, string[]>? errors = null) =>
        new() { Type = type, Errors = errors };
}