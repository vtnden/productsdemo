using ProductsDemo.UseCases.Handlers.Products.Dto;
using ProductsDemo.UseCases.Interfaces.Requests.Products;

namespace ProductsDemo.UseCases.Handlers.Products.Commands.UpdateProduct;

/// <summary>
/// Command for updating product
/// </summary>
/// <param name="Id"></param>
/// <param name="Value"></param>
public record UpdateProductCommand(int Id, CreateProductDto Value): IEditProductCommand;