# ProductsDemo

## Test project for creating products

## Instruction

#### 1. Execute in your shell

```shell
  docker compose -f docker-compose.local.yml up --build
```

#### 2. Go to swagger http://localhost:5000/swagger/index.html


## Project structure

- ### ProductsDemo.Api
- ### ProductsDemo.Domain
- ### ProductsDemo.OutboxService 
- ### ProductsDemo.Infrastructure.Elasticsearch
- ### ProductsDemo.Infrastructure.Postgres
- ### ProductsDemo.Infrastructure.UnitTests
- ### ProductsDemo.Infrastructure.UseCases
- ### ProductsDemo.Infrastructure.Utils

## Environmental variables for ProductsDemo.Api

- ASPNETCORE_ENVIRONMENT=Development
- ASPNETCORE_URLS=http://0.0.0.0:5000
- DB_HOST=products-db
- DB_PORT=5432
- DB_USER=postgres
- DB_PASSWORD=postgres
- DB_NAME=products
- ELASTIC_HOST=http://products-elasticsearch:9200/
- ELASTIC_USERNAME=elastic
- ELASTIC_PASSWORD=elastic
- ELASTIC_PRODUCTS_INDEX_NAME=products_idx
- ELASTIC_PRODUCTS_CATEGORIES_INDEX_NAME=products_categories_idx

## Environmental variables for ProductsDemo.OutboxService

- DOTNET_ENVIRONMENT=Development
- DB_HOST=products-db
- DB_PORT=5432
- DB_USER=postgres
- DB_PASSWORD=postgres
- DB_NAME=products
- ELASTIC_HOST=http://products-elasticsearch:9200/
- ELASTIC_USERNAME=elastic
- ELASTIC_PASSWORD=elastic
- ELASTIC_PRODUCTS_INDEX_NAME=products_idx
- ELASTIC_PRODUCTS_CATEGORIES_INDEX_NAME=products_categories_idx


## Success

- #### [v] .NET 7

- #### [v] EF Core Postgres

- #### [v] Elasticsearch

- #### [v] Nlog

- #### [v] Swagger

- #### [v] FluentValidation

- #### [v] Clean architecture (CQRS)

- #### [v] Unit Testing (Validators)

- #### [v] Api for products categories (create, get, search, update, delete)

- #### [v] Api for products (create, get, search, update, delete)

- #### [v] Outbox worker for products categories (bulk create, bulk update, bulk delete)

- #### [v] Outbox worker for products (bulk create, bulk update, bulk delete)


