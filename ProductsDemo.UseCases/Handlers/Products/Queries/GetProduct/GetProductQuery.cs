using ProductsDemo.UseCases.Interfaces.Requests;

namespace ProductsDemo.UseCases.Handlers.Products.Queries.GetProduct;

public record GetProductQuery(int Id): IQueryRequest;