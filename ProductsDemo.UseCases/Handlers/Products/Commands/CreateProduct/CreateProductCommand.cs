using ProductsDemo.UseCases.Handlers.Products.Dto;
using ProductsDemo.UseCases.Interfaces.Requests.Products;

namespace ProductsDemo.UseCases.Handlers.Products.Commands.CreateProduct;

/// <summary>
/// Command for creating a new product
/// </summary>
/// <param name="Value"></param>
public record CreateProductCommand(CreateProductDto Value): IEditProductCommand;