namespace ProductsDemo.UseCases.Handlers.Products.Dto;

public record ProductDto(int Id, string Name, string CategoryId, string Category, DateTimeOffset CreatedAt,
    DateTimeOffset? ModifiedAt,
    IReadOnlyCollection<ProductFieldDto>? Fields);