using ProductsDemo.Domain.CommonEntities;
using ProductsDemo.Domain.Enums;

namespace ProductsDemo.Infrastructure.Elasticsearch.Entities.ProductCategory;

public record ProductCategoryMultiLangField(string Id, string Name, string NameEn, FieldType FieldType, bool Required,
    IReadOnlyCollection<FieldProp>? Props);