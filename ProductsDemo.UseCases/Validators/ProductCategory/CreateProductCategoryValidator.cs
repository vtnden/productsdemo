using ProductsDemo.UseCases.Handlers.ProductsCategories.Commands.CreateProductCategory;
using ProductsDemo.UseCases.Validators.ProductCategory.Base;

namespace ProductsDemo.UseCases.Validators.ProductCategory;

public class CreateProductCategoryValidator: EditProductCategoryValidator<CreateProductCategoryCommand> {

}