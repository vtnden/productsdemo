using Elastic.Clients.Elasticsearch;
using Elastic.Clients.Elasticsearch.Analysis;
using Elastic.Clients.Elasticsearch.IndexManagement;
using Polly;
using ProductsDemo.Domain.Constants;
using ProductsDemo.Infrastructure.Elasticsearch.Entities.Product;
using ProductsDemo.Infrastructure.Elasticsearch.Extensions;
using ExistsResponse = Elastic.Clients.Elasticsearch.IndexManagement.ExistsResponse;

namespace ProductsDemo.Infrastructure.Elasticsearch.Migrations.Product;

public static class ProductIndexMigration {
    public static void CheckProductIndex(ElasticsearchClientSettings settings, ElasticsearchClient client) {
        var indexName = Environment.GetEnvironmentVariable(EnvConstants.ElasticProductsIndexName)!;

        settings.DefaultMappingFor<ProductMultiLang>(mapping => {
            mapping.IdProperty(x => x.Id);
            mapping.IndexName(indexName);
        });

        Policy.Handle<Exception>()
            .OrResult<(ExistsResponse, CreateIndexResponse)>(r => !r.Item1.IsSuccess())
            .WaitAndRetry(new[] { TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(5), TimeSpan.FromSeconds(5) })
            .Execute(() => {

                var existResponse =
                    client.Indices.Exists(
                        new Elastic.Clients.Elasticsearch.IndexManagement.ExistsRequestDescriptor<ProductMultiLang>(
                            indexName));

                var createResponse = CreateProductIndex(client, indexName);

                return (existResponse, createResponse);
            });

    }

    private static CreateIndexResponse CreateProductIndex(ElasticsearchClient client, string indexName) {
        return client.Indices.Create<ProductMultiLang>(indexName,
            index => {
                index.Settings(s => s.AddAnalyzers(Language.Russian, Language.English));
                index.Mappings(mapping => {
                    mapping.Properties(props => {
                        props.Keyword(x => x.Id);
                        props.Text(x => x.Name, x => x.UseAnalyzer(Language.Russian));
                        props.Text(x => x.NameEn, x => x.UseAnalyzer(Language.English));
                        props.Text(x => x.Category, x => x.UseAnalyzer(Language.Russian));
                        props.Text(x => x.CategoryEn, x => x.UseAnalyzer(Language.English));
                        props.Date(x => x.CreatedAt);
                        props.Date(x => x.ModifiedAt!);
                        props.Keyword(x => x.CategoryId);
                        props.Nested(x => x.Fields!,
                            n => n.Properties(np =>
                                np.Keyword(npp => npp.Fields!.First().FieldId)
                                    .Keyword(npp => npp.Fields!.First().FieldType)
                                    .Text(npp => npp.Fields!.First().Field, x => x.UseAnalyzer(Language.Russian))
                                    .Text(npp => npp.Fields!.First().FieldEn, x => x.UseAnalyzer(Language.English))
                                    .Text(npp => npp.Fields!.First().Value!, x => x.UseAnalyzer(Language.Russian))
                                    .Text(npp => npp.Fields!.First().ValueEn!, x => x.UseAnalyzer(Language.English))
                                    .Boolean(npp => npp.Fields!.First().Required)
                            )
                        );
                    });
                });
            });
    }
}