namespace ProductsDemo.UseCases.Interfaces.DB;

/// <summary>
/// Database interface with full access
/// </summary>
public interface IRwDbContext: IDbContext {
    void Migrate();
    Task<int> SaveChangesAsync(CancellationToken cancellationToken = new());
}