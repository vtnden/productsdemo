using FluentValidation;
using FluentValidation.Results;
using ProductsDemo.Domain.Common;
using ProductsDemo.Domain.CommonEntities;
using ProductsDemo.Domain.Enums;
using ProductsDemo.UseCases.Handlers.ProductsCategories.Dto;
using ProductsDemo.UseCases.Interfaces.Requests.Products;
using ProductsDemo.UseCases.Interfaces.Services;
using ProductsDemo.UseCases.Interfaces.Validators;

namespace ProductsDemo.UseCases.Validators.Products.Base;

/// <summary>
/// Basic validator for updating product
/// </summary>
/// <typeparam name="TCommand"></typeparam>
public abstract class EditProductValidatorBase<TCommand>: AbstractValidator<TCommand>
    where TCommand : IEditProductCommand {
    private readonly IProductCategoryService _productCategoryService;

    private readonly IValidationContext<(Dictionary<string, ProductCategoryField>? dict, ProductCategoryDto category)?>
        _validationContext;

    protected EditProductValidatorBase(IProductCategoryService productCategoryService,
        IValidationContext<(Dictionary<string, ProductCategoryField>? dict, ProductCategoryDto category)?>
            validationContext) {
        _productCategoryService = productCategoryService;
        _validationContext = validationContext;

        RuleFor(x => x.Value.Name).NotNull().NotEmpty();
        RuleFor(x => x.Value.CategoryId).NotNull().NotEmpty().Must(x => x == GetCategoryId());
        RuleForEach(x => x.Value.Fields).SetValidator((_, f) => {
            ProductCategoryField? field = null;
            _validationContext.Value?.dict?.TryGetValue(f.FieldId, out field);
            return new ProductFieldValidator(field);
        });
        return;

        string GetCategoryId() {
            return _validationContext.Value?.category.Id ?? "";
        }
    }

    public override async Task<ValidationResult> ValidateAsync(ValidationContext<TCommand> context,
        CancellationToken cancellation = new()) {
        var createProductDto = context.InstanceToValidate.Value;
        var categoryResult = await _productCategoryService.Get(createProductDto.CategoryId);

        var category = (categoryResult as Result<ProductCategoryDto?>.Ok)?.Value;

        if (category == null) {
            return new ValidationResult(new[]
                { new ValidationFailure(ErrorType.ProductCategoryNotFound.ToString(), "") });
        }

        _validationContext.Value = (category.Fields?.ToDictionary(x => x.Id), category);
        return await base.ValidateAsync(context, cancellation);
    }

}