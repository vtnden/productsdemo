using ProductsDemo.UseCases.Interfaces.Requests;

namespace ProductsDemo.UseCases.Handlers.ProductsCategories.Queries.GetProductCategory;

public record GetProductCategoryQuery(string Id): IQueryRequest;