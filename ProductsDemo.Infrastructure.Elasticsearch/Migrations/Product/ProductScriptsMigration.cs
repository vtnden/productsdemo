using System.Diagnostics;
using Elastic.Clients.Elasticsearch;
using ProductsDemo.Infrastructure.Elasticsearch.Constants;
using ProductsDemo.Infrastructure.Elasticsearch.Entities.Product;
using ProductsDemo.Infrastructure.Elasticsearch.Extensions;

namespace ProductsDemo.Infrastructure.Elasticsearch.Migrations.Product;

public static class ProductScriptsMigration {
    public static void CheckProductScripts(ElasticsearchClient client) {
        var response = CreateProductSearchScript(client);
        Debug.Assert(response.IsSuccess());
    }

    private static PutScriptResponse CreateProductSearchScript(ElasticsearchClient client) {
        var query = new SearchRequestDescriptor<ProductMultiLang>()
            .Query(q =>
                q.Bool(b =>
                    b.Should(
                        s => s.Match(
                            m => m.Field(f => f.Id)
                                .Query($"{{{{{QueryConstants.ProductSearchScriptTextParamName}}}}}")),
                        s => s.Match(
                            m => m.Field(f => f.Name)
                                .Query($"{{{{{QueryConstants.ProductSearchScriptTextParamName}}}}}")
                                .Fuzziness(QueryConstants.FuzzinessOne)),
                        s => s.Match(
                            m => m.Field(f => f.NameEn)
                                .Query($"{{{{{QueryConstants.ProductSearchScriptTextParamName}}}}}")
                                .Fuzziness(QueryConstants.FuzzinessOne)),
                        s => s.Match(
                            m => m.Field(f => f.Category)
                                .Query($"{{{{{QueryConstants.ProductSearchScriptTextParamName}}}}}")
                                .Fuzziness(QueryConstants.FuzzinessOne)),
                        s => s.Match(
                            m => m.Field(f => f.CategoryEn)
                                .Query($"{{{{{QueryConstants.ProductSearchScriptTextParamName}}}}}")
                                .Fuzziness(QueryConstants.FuzzinessOne)),
                        s => s.Nested(n => n.Path(p => p.Fields).Query(nq => nq.Bool(
                                    bb => bb.Should(
                                        bs => bs.Match(m =>
                                            m.Field(f => f.Fields!.First().FieldId)
                                                .Query($"{{{{{QueryConstants.ProductSearchScriptTextParamName}}}}}")),
                                        bs => bs.Match(m =>
                                            m.Field(f => f.Fields!.First().FieldType)
                                                .Query($"{{{{{QueryConstants.ProductSearchScriptTextParamName}}}}}")),
                                        bs => bs.Match(m =>
                                            m.Field(f => f.Fields!.First().Field)
                                                .Query($"{{{{{QueryConstants.ProductSearchScriptTextParamName}}}}}")
                                                .Fuzziness(QueryConstants.FuzzinessOne)),
                                        bs => bs.Match(m =>
                                            m.Field(f => f.Fields!.First().FieldEn)
                                                .Query($"{{{{{QueryConstants.ProductSearchScriptTextParamName}}}}}")
                                                .Fuzziness(QueryConstants.FuzzinessOne)),
                                        bs => bs.Match(m =>
                                            m.Field(f => f.Fields!.First().Value)
                                                .Query($"{{{{{QueryConstants.ProductSearchScriptTextParamName}}}}}")
                                                .Fuzziness(QueryConstants.FuzzinessOne)),
                                        bs => bs.Match(m =>
                                            m.Field(f => f.Fields!.First().ValueEn)
                                                .Query($"{{{{{QueryConstants.ProductSearchScriptTextParamName}}}}}")
                                                .Fuzziness(QueryConstants.FuzzinessOne))
                                    )
                                )
                            )
                        )
                    )
                )
            );

        return client.UploadScript(QueryConstants.ProductSearchScriptName, query);

    }
}