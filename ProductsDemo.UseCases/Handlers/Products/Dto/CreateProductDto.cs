using ProductsDemo.Domain.CommonEntities;

namespace ProductsDemo.UseCases.Handlers.Products.Dto;

public record CreateProductDto(string Name, string CategoryId, IReadOnlyCollection<ProductField>? Fields);