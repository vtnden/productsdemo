using ProductsDemo.Domain.Enums;

namespace ProductsDemo.Domain.CommonEntities;

public record ProductCategoryField(string Id, string Name, FieldType FieldType, bool Required,
    IReadOnlyCollection<FieldProp>? Props);