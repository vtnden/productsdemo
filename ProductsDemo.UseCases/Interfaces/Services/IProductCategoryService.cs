using ProductsDemo.Domain.Common;
using ProductsDemo.UseCases.Handlers.ProductsCategories.Dto;

namespace ProductsDemo.UseCases.Interfaces.Services;

public interface IProductCategoryService {
    Task<Result<ProductCategoryDto?>> Get(string id);
    Task<IReadOnlyCollection<Result<bool>>> Create(IReadOnlyCollection<ProductCategoryDto> categories);
    Task<Result<IReadOnlyCollection<ProductCategoryDto>>> Search(string? text);
    Task<IReadOnlyCollection<Result<bool>>> Update(IReadOnlyCollection<ProductCategoryDto> categories);
    Task<IReadOnlyCollection<Result<bool>>> Delete(IReadOnlyCollection<string> categoryIds);
}