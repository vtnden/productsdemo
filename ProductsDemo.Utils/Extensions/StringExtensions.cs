namespace ProductsDemo.Utils.Extensions;

public static class StringExtensions {
    public static string ToFirstLetterAsLower(this string str) {
        return char.ToLower(str[0]) + str[1..];
    }
}