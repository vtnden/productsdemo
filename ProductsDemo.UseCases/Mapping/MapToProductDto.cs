using Mapster;
using ProductsDemo.Domain.CommonEntities;
using ProductsDemo.UseCases.Handlers.Products.Dto;
using ProductsDemo.UseCases.Handlers.ProductsCategories.Dto;

namespace ProductsDemo.UseCases.Mapping;

/// <summary>
/// Mapping for CreateProductDto -> ProductDto
/// </summary>
public class MapToProductDto: IMapFrom<(CreateProductDto createDto, Dictionary<string, ProductCategoryField>? dict,
    ProductCategoryDto
    category)> {
    public void ConfigureMapping(TypeAdapterConfig config) {
        config
            .NewConfig<(CreateProductDto createDto, Dictionary<string, ProductCategoryField>? dict, ProductCategoryDto
                category), ProductDto>()
            .Map(x => x.Name, source => source.createDto.Name)
            .Map(x => x.CategoryId, source => source.createDto.CategoryId)
            .Map(x => x.Category, source => source.category.Name)
            .Map(x => x.Fields,
                source => source.createDto.Fields!.Select(f => new ProductFieldDto(source.dict![f.FieldId].Required,
                        source.dict[f.FieldId].FieldType, f.FieldId, source.dict[f.FieldId].Name, f.Value))
                    .ToList())
            .IgnoreNullValues(true);
    }
}