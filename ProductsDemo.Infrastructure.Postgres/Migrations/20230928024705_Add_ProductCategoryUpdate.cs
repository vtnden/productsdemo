﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ProductsDemo.Infrastructure.Postgres.Migrations
{
    /// <inheritdoc />
    public partial class Add_ProductCategoryUpdate : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterDatabase()
                .Annotation("Npgsql:Enum:operation_type", "create,update")
                .OldAnnotation("Npgsql:Enum:operation_type", "create");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterDatabase()
                .Annotation("Npgsql:Enum:operation_type", "create")
                .OldAnnotation("Npgsql:Enum:operation_type", "create,update");
        }
    }
}
