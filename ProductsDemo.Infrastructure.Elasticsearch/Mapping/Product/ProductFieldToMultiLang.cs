using Mapster;
using ProductsDemo.Infrastructure.Elasticsearch.Entities.Product;
using ProductsDemo.UseCases.Handlers.Products.Dto;

namespace ProductsDemo.Infrastructure.Elasticsearch.Mapping.Product;

public class ProductFieldToMultiLang: IMapFrom<ProductFieldDto> {
    public void ConfigureMapping(TypeAdapterConfig config) {
        config
            .NewConfig<ProductFieldDto, ProductMultiLangField>()
            .Map(x => x.FieldEn, source => source.Field)
            .Map(x => x.ValueEn, source => source.Value);
    }
}