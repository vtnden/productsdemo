using System.Text.Json;
using ProductsDemo.Domain.Entities.Base;

namespace ProductsDemo.Domain.Entities;

public class ProductCategory: EntityBase<string> {
    public string Name { get; set; } = default!;
    public JsonDocument? Fields { get; set; }
}