using Microsoft.AspNetCore.Mvc;
using ProductsDemo.Api.Controllers.Base;
using ProductsDemo.Api.Extensions;
using ProductsDemo.Domain.Common;
using ProductsDemo.UseCases.Handlers.Products.Commands.CreateProduct;
using ProductsDemo.UseCases.Handlers.Products.Commands.DeleteProduct;
using ProductsDemo.UseCases.Handlers.Products.Commands.UpdateProduct;
using ProductsDemo.UseCases.Handlers.Products.Dto;
using ProductsDemo.UseCases.Handlers.Products.Queries.GetProduct;
using ProductsDemo.UseCases.Handlers.Products.Queries.SearchProducts;
using ProductsDemo.UseCases.Interfaces.Handlers;

namespace ProductsDemo.Api.Controllers;

/// <summary>
/// Api for product 
/// </summary>
public class ProductController: ApiControllerBase {
    private readonly IRequestHandler<GetProductQuery, ProductDto?> _getHandler;
    private readonly IRequestHandler<CreateProductCommand, bool> _createHandler;
    private readonly IRequestHandler<UpdateProductCommand, bool> _updateHandler;
    private readonly IRequestHandler<DeleteProductCommand, bool> _deleteHandler;

    private readonly IRequestHandler<SearchProductsQuery, IReadOnlyCollection<ProductDto>>
        _searchHandler;

    public ProductController(IRequestHandler<CreateProductCommand, bool> createHandler,
        IRequestHandler<GetProductQuery, ProductDto?> getHandler,
        IRequestHandler<SearchProductsQuery, IReadOnlyCollection<ProductDto>> searchHandler,
        IRequestHandler<UpdateProductCommand, bool> updateHandler,
        IRequestHandler<DeleteProductCommand, bool> deleteHandler) {
        _createHandler = createHandler;
        _getHandler = getHandler;
        _searchHandler = searchHandler;
        _updateHandler = updateHandler;
        _deleteHandler = deleteHandler;
    }

    /// <summary>
    /// Get product by id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Result<ProductDto?>.Ok))]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<ProductDto?>.Error))]
    [HttpGet("{id:int}")]
    public async Task<JsonResult> Get(int id) {
        var result = await _getHandler.Send(new GetProductQuery(id));
        return result.AsJsonResult();
    }

    /// <summary>
    /// Search products by text
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Result<IReadOnlyCollection<ProductDto>>.Ok))]
    [ProducesResponseType(StatusCodes.Status500InternalServerError,
        Type = typeof(Result<IReadOnlyCollection<ProductDto>>.Error))]
    [HttpGet("search")]
    public async Task<JsonResult> Search(string? text) {
        var result = await _searchHandler.Send(new SearchProductsQuery(text));
        return result.AsJsonResult();
    }

    /// <summary>
    /// Create a new product  
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Result<bool>.Ok))]
    [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<bool>.Error))]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<bool>.Error))]
    [HttpPost]
    public async Task<JsonResult> Create([FromBody] CreateProductDto value) {
        var result = await _createHandler.Send(new CreateProductCommand(value));
        return result.AsJsonResult();
    }

    /// <summary>
    /// Update product 
    /// </summary>
    /// <param name="id"></param>
    /// <param name="value"></param>
    /// <returns></returns>
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Result<bool>.Ok))]
    [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Result<bool>.Error))]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<bool>.Error))]
    [HttpPut("{id:int}")]
    public async Task<JsonResult> Update([FromRoute] int id, [FromBody] CreateProductDto value) {
        var result = await _updateHandler.Send(new UpdateProductCommand(id, value));
        return result.AsJsonResult();
    }

    /// <summary>
    /// Delete product 
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Result<bool>.Ok))]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(Result<bool>.Error))]
    [HttpDelete("{id:int}")]
    public async Task<JsonResult> Delete([FromRoute] int id) {
        var result = await _deleteHandler.Send(new DeleteProductCommand(id));
        return result.AsJsonResult();
    }
}