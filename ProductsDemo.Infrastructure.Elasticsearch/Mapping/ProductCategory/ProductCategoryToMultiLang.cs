using Mapster;
using ProductsDemo.Infrastructure.Elasticsearch.Entities.ProductCategory;
using ProductsDemo.UseCases.Handlers.ProductsCategories.Dto;

namespace ProductsDemo.Infrastructure.Elasticsearch.Mapping.ProductCategory;

public class ProductCategoryToMultiLang: IMapFrom<ProductCategoryDto> {
    public void ConfigureMapping(TypeAdapterConfig config) {
        config
            .NewConfig<ProductCategoryDto, ProductCategoryMultiLang>()
            .Map(x => x.NameEn, source => source.Name)
            .Map(x => x.Fields, source => source.Fields!.Select(x => x.Adapt<ProductCategoryMultiLangField>()).ToList())
            .IgnoreNullValues(true);
    }
}