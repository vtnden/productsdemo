using System.Text.Json;
using ProductsDemo.Domain.Entities.Base;

namespace ProductsDemo.Domain.Entities;

public class Product: EntityBase<int> {
    public string Name { get; set; } = default!;
    public string CategoryId { get; set; } = default!;
    public ProductCategory? Category { get; set; }
    public JsonDocument? Fields { get; set; }
}