using System.Transactions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using ProductsDemo.Domain.Entities;

namespace ProductsDemo.UseCases.Interfaces.DB;

/// <summary>
/// Database interface for readonly using
/// </summary>
public interface IDbContext {
    public DbSet<Product> Products { get; set; }
    public DbSet<ProductCategory> ProductCategories { get; set; }
    public DbSet<ProductCategoryOutbox> ProductCategoryOutbox { get; set; }
    public DbSet<ProductOutbox> ProductOutbox { get; set; }
    Task<IDbContextTransaction> CreateTransactionScope(IsolationLevel isolationLevel);
}