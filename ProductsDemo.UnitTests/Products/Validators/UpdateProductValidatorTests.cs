using FluentAssertions;
using ProductsDemo.Domain.Common;
using ProductsDemo.Domain.CommonEntities;
using ProductsDemo.Domain.Enums;
using ProductsDemo.UnitTests.Utils;
using ProductsDemo.UseCases.Handlers.Products.Commands.UpdateProduct;
using ProductsDemo.UseCases.Handlers.Products.Dto;
using ProductsDemo.UseCases.Handlers.ProductsCategories.Dto;
using ProductsDemo.UseCases.Validators.Products;

namespace ProductsDemo.UnitTests.Products.Validators;

public class UpdateProductValidatorTests {
    [Fact]
    public async Task UpdateProduct_WithValidName_WithoutFields_ReturnTrue() {
        var service = MockUtils.MockProductCategoryServiceWithDto(Result<ProductCategoryDto?>
            .FromResult(new ProductCategoryDto(
                "1", "Name", null,
                DateTimeOffset.Now, DateTimeOffset.Now)));
        var context = MockUtils
            .MockValidationContext<(Dictionary<string, ProductCategoryField>? dict, ProductCategoryDto category)?>();

        var validator = new UpdateProductValidator(service, context);
        var product = new UpdateProductCommand(1, new CreateProductDto("1", "1", null));

        var result = await validator.ValidateAsync(product);

        result.IsValid.Should().BeTrue();
    }

    [Fact]
    public async Task UpdateProduct_WithInvalidName_WithoutFields_ReturnFalse() {
        var service = MockUtils.MockProductCategoryServiceWithDto(Result<ProductCategoryDto?>
            .FromResult(new ProductCategoryDto(
                "1", "Name", null,
                DateTimeOffset.Now, DateTimeOffset.Now)));
        var context = MockUtils
            .MockValidationContext<(Dictionary<string, ProductCategoryField>? dict, ProductCategoryDto category)?>();

        var validator = new UpdateProductValidator(service, context);
        var product = new UpdateProductCommand(1, new CreateProductDto("1", "", null));

        var result = await validator.ValidateAsync(product);

        result.IsValid.Should().BeFalse();
    }

    [Fact]
    public async Task UpdateProduct_WithValidName_WithValidTextField_ReturnTrue() {
        var service = MockUtils.MockProductCategoryServiceWithDto(Result<ProductCategoryDto?>
            .FromResult(new ProductCategoryDto(
                "1", "Name", new List<ProductCategoryField>() {
                    new("1", "f1", FieldType.Text, true, new List<FieldProp>() {
                        new(FieldPropType.Max, "10"),
                    }),
                },
                DateTimeOffset.Now, DateTimeOffset.Now)));
        var context = MockUtils
            .MockValidationContext<(Dictionary<string, ProductCategoryField>? dict, ProductCategoryDto category)?>();

        var validator = new UpdateProductValidator(service, context);
        var product = new UpdateProductCommand(1, new CreateProductDto("1", "1", new List<ProductField>() {
            new("1", "value"),
        }));

        var result = await validator.ValidateAsync(product);

        result.IsValid.Should().BeTrue();
    }

    [Fact]
    public async Task UpdateProduct_WithValidName_WithValidTextIdField_ReturnFalse() {
        var service = MockUtils.MockProductCategoryServiceWithDto(Result<ProductCategoryDto?>
            .FromResult(new ProductCategoryDto(
                "1", "Name", new List<ProductCategoryField>() {
                    new("1", "f1", FieldType.Text, true, new List<FieldProp>() {
                        new(FieldPropType.Max, "10"),
                    }),
                },
                DateTimeOffset.Now, DateTimeOffset.Now)));
        var context = MockUtils
            .MockValidationContext<(Dictionary<string, ProductCategoryField>? dict, ProductCategoryDto category)?>();

        var validator = new UpdateProductValidator(service, context);
        var product = new UpdateProductCommand(1, new CreateProductDto("1", "Name", new List<ProductField>() {
            new("2", "value"),
        }));

        var result = await validator.ValidateAsync(product);

        result.IsValid.Should().BeFalse();
    }

    [Fact]
    public async Task UpdateProduct_WithValidName_WithValidTextField_WithInvalidTextFieldMaxLength_ReturnFalse() {
        var service = MockUtils.MockProductCategoryServiceWithDto(Result<ProductCategoryDto?>
            .FromResult(new ProductCategoryDto(
                "1", "Name", new List<ProductCategoryField>() {
                    new("1", "f1", FieldType.Text, true, new List<FieldProp>() {
                        new(FieldPropType.Max, "2"),
                    }),
                },
                DateTimeOffset.Now, DateTimeOffset.Now)));
        var context = MockUtils
            .MockValidationContext<(Dictionary<string, ProductCategoryField>? dict, ProductCategoryDto category)?>();

        var validator = new UpdateProductValidator(service, context);
        var product = new UpdateProductCommand(1, new CreateProductDto("1", "Name", new List<ProductField>() {
            new("1", "value"),
        }));

        var result = await validator.ValidateAsync(product);

        result.IsValid.Should().BeFalse();
    }

    [Fact]
    public async Task UpdateProduct_WithValidName_WithValidTwoTextFields_ReturnTrue() {
        var service = MockUtils.MockProductCategoryServiceWithDto(Result<ProductCategoryDto?>
            .FromResult(new ProductCategoryDto(
                "1", "Name", new List<ProductCategoryField>() {
                    new("1", "f1", FieldType.Text, true, new List<FieldProp>() {
                        new(FieldPropType.Max, "5"),
                    }),
                    new("2", "f2", FieldType.Text, true, new List<FieldProp>() {
                        new(FieldPropType.Max, "100"),
                    }),
                },
                DateTimeOffset.Now, DateTimeOffset.Now)));
        var context = MockUtils
            .MockValidationContext<(Dictionary<string, ProductCategoryField>? dict, ProductCategoryDto category)?>();

        var validator = new UpdateProductValidator(service, context);
        var product = new UpdateProductCommand(1, new CreateProductDto("1", "1", new List<ProductField>() {
            new("1", "value"),
            new("2", "value max"),
        }));

        var result = await validator.ValidateAsync(product);

        result.IsValid.Should().BeTrue();
    }

}