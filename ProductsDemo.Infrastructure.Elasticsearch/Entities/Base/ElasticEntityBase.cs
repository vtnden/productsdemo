namespace ProductsDemo.Infrastructure.Elasticsearch.Entities.Base;

public abstract class ElasticEntityBase<TId> {
    public required TId Id { get; init; }
    public DateTimeOffset CreatedAt { get; init; }
    public DateTimeOffset? ModifiedAt { get; init; }
}