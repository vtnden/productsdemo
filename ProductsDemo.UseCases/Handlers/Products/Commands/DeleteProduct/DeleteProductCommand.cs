using ProductsDemo.UseCases.Interfaces.Requests;

namespace ProductsDemo.UseCases.Handlers.Products.Commands.DeleteProduct;

public record DeleteProductCommand(int Id): ICommandRequest;