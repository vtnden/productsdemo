using ProductsDemo.Domain.CommonEntities;

namespace ProductsDemo.UseCases.Interfaces.Requests.ProductCategory;

public interface IEditProductCategoryCommand: ICommandRequest {
    string Id { get; }
    string Name { get; }
    IReadOnlyCollection<ProductCategoryField>? Fields { get; }
}