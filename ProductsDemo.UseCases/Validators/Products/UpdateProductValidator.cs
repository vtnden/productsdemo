using ProductsDemo.Domain.CommonEntities;
using ProductsDemo.UseCases.Handlers.Products.Commands.UpdateProduct;
using ProductsDemo.UseCases.Handlers.ProductsCategories.Dto;
using ProductsDemo.UseCases.Interfaces.Services;
using ProductsDemo.UseCases.Interfaces.Validators;
using ProductsDemo.UseCases.Validators.Products.Base;

namespace ProductsDemo.UseCases.Validators.Products;

public class UpdateProductValidator: EditProductValidatorBase<UpdateProductCommand> {

    public UpdateProductValidator(IProductCategoryService productCategoryService,
        IValidationContext<(Dictionary<string, ProductCategoryField>? dict, ProductCategoryDto category)?>
            validationContext): base(productCategoryService, validationContext) {
    }
}