using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection;
using FluentValidation;
using Mapster;
using Mapster.Utils;
using Microsoft.Extensions.DependencyInjection;
using ProductsDemo.Domain.CommonEntities;
using ProductsDemo.UseCases.Handlers.ProductsCategories.Dto;
using ProductsDemo.UseCases.Interfaces.Handlers;
using ProductsDemo.UseCases.Interfaces.Validators;
using ProductsDemo.UseCases.Pipelines;
using ProductsDemo.UseCases.Validators;

namespace ProductsDemo.UseCases;

/// <summary>
/// Main application module 
/// </summary>
public static class UseCasesModule {
    public static void Init(IServiceCollection serviceCollection) {
        serviceCollection.Scan(scan =>
            scan.FromCallingAssembly()
                .AddClasses(
                    classes => classes.AssignableTo(typeof(IValidator<>)).WithoutAttribute<NotMappedAttribute>()
                        .Where(x => !x.IsAbstract))
                .AsImplementedInterfaces()
                .WithScopedLifetime()
                .AddClasses(classes => classes.AssignableTo(typeof(IRequestHandler<,>)))
                .AsImplementedInterfaces()
                .WithScopedLifetime()
        );

        serviceCollection.Decorate(typeof(IRequestHandler<,>),
            typeof(RequestPipelineValidator<,>));

        serviceCollection
            .AddScoped<IValidationContext<(Dictionary<string, ProductCategoryField>?, ProductCategoryDto)?>,
                CommonValidationContext<(Dictionary<string, ProductCategoryField>?, ProductCategoryDto)?>>();

        TypeAdapterConfig.GlobalSettings.ScanInheritedTypes(Assembly.GetExecutingAssembly());
    }
}