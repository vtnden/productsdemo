using ProductsDemo.Domain.Common;
using ProductsDemo.UseCases.Handlers.Products.Dto;
using ProductsDemo.UseCases.Interfaces.Handlers;
using ProductsDemo.UseCases.Interfaces.Services;

namespace ProductsDemo.UseCases.Handlers.Products.Queries.SearchProducts;

public class SearchProductsHandler: IQueryHandler<SearchProductsQuery, IReadOnlyCollection<ProductDto>> {
    private readonly IProductService _service;

    public SearchProductsHandler(IProductService service) {
        _service = service;
    }

    public Task<Result<IReadOnlyCollection<ProductDto>>> Send(SearchProductsQuery request,
        CancellationToken cancellationToken = default) {
        return _service.Search(request.Text);
    }
}