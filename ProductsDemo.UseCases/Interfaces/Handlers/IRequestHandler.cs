using ProductsDemo.Domain.Common;
using ProductsDemo.UseCases.Interfaces.Requests;

namespace ProductsDemo.UseCases.Interfaces.Handlers;

/// <summary>
/// Interface for all requests handlers
/// </summary>
/// <typeparam name="TRequest"></typeparam>
/// <typeparam name="TResult"></typeparam>
public interface IRequestHandler<in TRequest, TResult> where TRequest : IRequest {
    /// <summary>
    /// Send request to handler
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    Task<Result<TResult>> Send(TRequest request, CancellationToken cancellationToken = default);
}