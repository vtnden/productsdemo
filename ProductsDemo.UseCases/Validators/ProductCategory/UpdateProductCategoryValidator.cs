using ProductsDemo.UseCases.Handlers.ProductsCategories.Commands.UpdateProductCategory;
using ProductsDemo.UseCases.Validators.ProductCategory.Base;

namespace ProductsDemo.UseCases.Validators.ProductCategory;

public class UpdateProductCategoryValidator: EditProductCategoryValidator<UpdateProductCategoryCommand> {

}