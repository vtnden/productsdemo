using System.Text.Json;
using Mapster;
using ProductsDemo.Domain.Constants;
using ProductsDemo.Domain.Entities;
using ProductsDemo.UseCases.Handlers.Products.Dto;

namespace ProductsDemo.UseCases.Mapping;

/// <summary>
/// Mapping for CreateProductDto -> Product
/// </summary>
public class MapToProductFromCommand: IMapFrom<CreateProductDto> {
    public void ConfigureMapping(TypeAdapterConfig config) {
        config
            .NewConfig<CreateProductDto, Product>()
            .Map(x => x.Fields,
                source => JsonDocument.Parse(JsonSerializer.Serialize(source.Fields,
                    CommonConstants.JsonOptionsWithStringEnum), new JsonDocumentOptions()))
            .IgnoreNullValues(true);
    }
}