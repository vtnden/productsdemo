using System.Text.Json;
using System.Transactions;
using Mapster;
using ProductsDemo.Domain.Common;
using ProductsDemo.Domain.CommonEntities;
using ProductsDemo.Domain.Constants;
using ProductsDemo.Domain.Entities;
using ProductsDemo.Domain.Enums;
using ProductsDemo.UseCases.Handlers.Products.Dto;
using ProductsDemo.UseCases.Handlers.ProductsCategories.Dto;
using ProductsDemo.UseCases.Interfaces.DB;
using ProductsDemo.UseCases.Interfaces.Handlers;
using ProductsDemo.UseCases.Interfaces.Validators;

namespace ProductsDemo.UseCases.Handlers.Products.Commands.CreateProduct;

/// <summary>
/// Handler for creating a new product
/// </summary>
public class CreateProductHandler: ICommandHandler<CreateProductCommand, bool> {
    private readonly IRwDbContext _dbContext;

    private readonly IValidationContext<(Dictionary<string, ProductCategoryField>? dict, ProductCategoryDto category)?>
        _validationContext;


    public CreateProductHandler(
        IValidationContext<(Dictionary<string, ProductCategoryField>?, ProductCategoryDto)?> validationContext,
        IRwDbContext dbContext) {
        _validationContext = validationContext;
        _dbContext = dbContext;
    }

    public async Task<Result<bool>> Send(CreateProductCommand request, CancellationToken cancellationToken = default) {
        await using var tr = await _dbContext.CreateTransactionScope(IsolationLevel.ReadCommitted);

        var product = request.Value.Adapt<Product>();
        _dbContext.Products.Add(product);
        await _dbContext.SaveChangesAsync(cancellationToken);

        var context = (request.Value, _validationContext.Value?.dict, _validationContext.Value?.category!);
        var productDto = context.Adapt<ProductDto>() with { Id = product.Id, CreatedAt = product.CreatedAt };

        _dbContext.ProductOutbox.Add(new ProductOutbox() {
            OperationType = OperationType.Create,
            Data = JsonDocument.Parse(JsonSerializer.Serialize(productDto,
                CommonConstants.JsonOptionsWithStringEnum)),
        });

        await _dbContext.SaveChangesAsync(cancellationToken);
        await tr.CommitAsync(cancellationToken);

        return Result<bool>.FromResult(true);
    }
}