namespace ProductsDemo.UseCases.Interfaces.Validators;

public interface IValidationContext<T> {
    T Value { get; set; }
}