using ProductsDemo.UseCases.Interfaces.Requests;

namespace ProductsDemo.UseCases.Interfaces.Handlers;

/// <summary>
/// Interface for all pipelines handlers
/// </summary>
/// <typeparam name="TRequest"></typeparam>
/// <typeparam name="TResult"></typeparam>
public interface IPipelineHandler<in TRequest, TResult>: IRequestHandler<TRequest, TResult> where TRequest : IRequest {
}