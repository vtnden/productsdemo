using FluentValidation;
using ProductsDemo.UseCases.Interfaces.Requests.ProductCategory;

namespace ProductsDemo.UseCases.Validators.ProductCategory.Base;

/// <summary>
/// Basic validator for updating product categories
/// </summary>
/// <typeparam name="TCommand"></typeparam>
public abstract class EditProductCategoryValidator<TCommand>: AbstractValidator<TCommand>
    where TCommand : IEditProductCategoryCommand {
    protected EditProductCategoryValidator() {
        RuleFor(x => x.Id).NotNull().NotEmpty();
        RuleFor(x => x.Name).NotNull().NotEmpty();

        RuleForEach(x => x.Fields)
            .ChildRules(field => {
                field.RuleFor(f => f.Id).NotNull().NotEmpty();
                field.RuleFor(f => f.Name).NotNull().NotEmpty();
                field.RuleForEach(fp => fp.Props).ChildRules(prop => {
                    var parseResult = 0;
                    prop.RuleFor(pp => pp.Value).NotNull().NotEmpty().Must(pp => int.TryParse(pp, out parseResult))
                        .DependentRules(
                            () => {
                                prop.RuleFor(ppp => ppp.FieldPropType).Must(type => type switch {
                                    _ => true,
                                });
                            });
                });
            });
    }
}