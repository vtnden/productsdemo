using System.Text.Json;
using ProductsDemo.Domain.Entities.Base;

namespace ProductsDemo.Domain.Entities;

/// <summary>
/// Outbox for all products
/// </summary>
public class ProductOutbox: OutboxBase<JsonDocument> {

}