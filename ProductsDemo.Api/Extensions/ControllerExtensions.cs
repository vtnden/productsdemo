using Microsoft.AspNetCore.Mvc;
using ProductsDemo.Domain.Common;

namespace ProductsDemo.Api.Extensions;

public static class ControllerExtensions {
    public static JsonResult AsJsonResult<TResult>(this Result<TResult> result) {
        var statusCode = result switch {
            Result<TResult>.Ok => StatusCodes.Status200OK,
            _ => StatusCodes.Status400BadRequest,
        };

        return new JsonResult(result) {
            StatusCode = statusCode,
        };
    }
}