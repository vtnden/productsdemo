using System.Text.Json;
using System.Transactions;
using Mapster;
using Microsoft.EntityFrameworkCore;
using ProductsDemo.Domain.Common;
using ProductsDemo.Domain.Constants;
using ProductsDemo.Domain.Entities;
using ProductsDemo.Domain.Enums;
using ProductsDemo.UseCases.Handlers.ProductsCategories.Dto;
using ProductsDemo.UseCases.Interfaces.DB;
using ProductsDemo.UseCases.Interfaces.Handlers;

namespace ProductsDemo.UseCases.Handlers.ProductsCategories.Commands.UpdateProductCategory;

public class UpdateProductCategoryHandler: IRequestHandler<UpdateProductCategoryCommand, bool> {
    private readonly IRwDbContext _dbContext;

    public UpdateProductCategoryHandler(IRwDbContext dbContext) {
        _dbContext = dbContext;
    }

    public async Task<Result<bool>> Send(UpdateProductCategoryCommand request, CancellationToken cancellationToken) {
        await using var tr = await _dbContext.CreateTransactionScope(IsolationLevel.ReadCommitted);
        var fields = JsonDocument.Parse(JsonSerializer.Serialize(request.Fields,
            CommonConstants.JsonOptionsWithStringEnum));

        var count = await _dbContext.ProductCategories.Where(x => x.Id == request.Id && x.DeletedAt == null)
            .ExecuteUpdateAsync(
                x =>
                    x.SetProperty(p => p.Name, request.Name)
                        .SetProperty(p => p.Fields,
                            fields)
                        .SetProperty(p => p.ModifiedAt, DateTimeOffset.Now),
                cancellationToken: cancellationToken);

        if (count == 0) {
            return Result<bool>.FromError(ErrorType.ProductCategoryNotFound);
        }

        var outBox = new ProductCategoryOutbox() {
            OperationType = OperationType.Update,
            Data = JsonDocument.Parse(JsonSerializer.Serialize(request.Adapt<ProductCategoryDto>(),
                CommonConstants.JsonOptionsWithStringEnum)),
        };
        _dbContext.ProductCategoryOutbox.Add(outBox);

        await _dbContext.SaveChangesAsync(cancellationToken);
        await tr.CommitAsync(cancellationToken);

        return Result<bool>.FromResult(true);
    }
}