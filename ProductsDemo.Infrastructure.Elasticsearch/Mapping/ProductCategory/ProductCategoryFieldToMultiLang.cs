using Mapster;
using ProductsDemo.Domain.CommonEntities;
using ProductsDemo.Infrastructure.Elasticsearch.Entities.ProductCategory;

namespace ProductsDemo.Infrastructure.Elasticsearch.Mapping.ProductCategory;

public class ProductCategoryFieldToMultiLang: IMapFrom<ProductCategoryField> {
    public void ConfigureMapping(TypeAdapterConfig config) {
        config
            .NewConfig<ProductCategoryField, ProductCategoryMultiLangField>()
            .Map(x => x.NameEn, source => source.Name);
    }
}