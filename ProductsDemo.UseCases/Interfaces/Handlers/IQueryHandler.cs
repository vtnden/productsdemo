using ProductsDemo.UseCases.Interfaces.Requests;

namespace ProductsDemo.UseCases.Interfaces.Handlers;

/// <summary>
/// Interface for all queries handlers
/// </summary>
/// <typeparam name="TRequest"></typeparam>
/// <typeparam name="TResult"></typeparam>
public interface IQueryHandler<in TRequest, TResult>: IRequestHandler<TRequest, TResult>
    where TRequest : IQueryRequest {

}