using ProductsDemo.Domain.Common;
using ProductsDemo.UseCases.Handlers.Products.Dto;

namespace ProductsDemo.UseCases.Interfaces.Services;

public interface IProductService {
    Task<Result<ProductDto?>> Get(int id);
    Task<IReadOnlyCollection<Result<bool>>> Create(IReadOnlyCollection<ProductDto> products);
    Task<Result<IReadOnlyCollection<ProductDto>>> Search(string? text);
    Task<IReadOnlyCollection<Result<bool>>> Update(IReadOnlyCollection<ProductDto> products);
    Task<IReadOnlyCollection<Result<bool>>> Delete(IReadOnlyCollection<int> productIds);
}