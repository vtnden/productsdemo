using ProductsDemo.Domain.Enums;

namespace ProductsDemo.UseCases.Handlers.Products.Dto;

public record ProductFieldDto(bool Required, FieldType FieldType, string FieldId, string Field, string? Value);