using ProductsDemo.UseCases.Interfaces.Validators;

namespace ProductsDemo.UseCases.Validators;

public class CommonValidationContext<T>: IValidationContext<T> {
    public T Value { get; set; } = default;
}