using System.Text.Json;
using Elastic.Clients.Elasticsearch;
using Elastic.Clients.Elasticsearch.Serialization;
using ProductsDemo.Domain.Constants;

namespace ProductsDemo.Infrastructure.Elasticsearch.Common;

public class ElasticCustomJsonSerializer: SystemTextJsonSerializer {
    public ElasticCustomJsonSerializer(IElasticsearchClientSettings settings): base(settings) {
    }

    protected override JsonSerializerOptions CreateJsonSerializerOptions() => CommonConstants.JsonOptionsWithStringEnum;
}